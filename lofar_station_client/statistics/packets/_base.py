#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-many-instance-attributes

"""Base classes for package definitions"""

from abc import abstractmethod
from datetime import datetime, timezone
from typing import TypeVar

from ._utils import get_bit_value

N_POL = 2
N_COMPLEX = 2

Subfields = TypeVar("Subfields")


class _BasePacketHeader:
    """Models a UDP packet from SDP.

    Packets are expected to be UDP payload only (so no Ethernet/IP/UDP headers).

    The following fields are exposed as properties & functions. The _raw fields come
    directly from the packet, and have more user-friendly alternatives for
    interpretation:

    marker_raw

        packet marker as byte.

    marker

        packet marker as character. 'S' = SST, 'X' = XST, 'B' = BST

    version_id

        packet format version.

    observation_id

        observation identifier.

    station_info

        bit field with station information, encoding several other properties.

    station_id

        station identifier.

    antenna_field_index

        antenna field id. 0 = LBA/HBA/HBA0, 1 = HBA1

    source_info

        bit field with input information, encoding several other properties.

    antenna_band_index

        antenna type. 0 = low band, 1 = high band.

    nyquist_zone_index

        nyquist zone of filter

            - 0 =           0 -- 1/2 * t_adc Hz (low band),
            - 1 = 1/2 * t_adc -- t_adc Hz       (high band),
            - 2 =       t_adc -- 3/2 * t_adc Hz (high band).

    t_adc

        sampling clock. 0 = 160 MHz, 1 = 200 MHz.

    fsub_type

        sampling method. 0 = critically sampled, 1 = oversampled.

    payload_error

        0 = data is ok, 1 = data is corrupted (a fault was encountered).

    beam_repositioning_flag

        0 = data is ok, 1 = beam got repositioned during packet construction
        (BST only).

    gn_index

        global index of FPGA that emitted this packet.

    block_period_raw

        block period, in nanoseconds.

    block_period

        block period, in seconds.

    block_serial_number

        timestamp of the data, in block periods since 1970.

    timestamp

        timestamp of the data, as a datetime object.

    """

    def __init__(self, packet: bytes):
        self.version_id: int = 0
        self.block_serial_number: int = 0
        self.block_period_raw: int = 0
        self.antenna_field_index: int = 0
        self.station_id: int = 0
        self.source_info: int = 0
        self.antenna_band_index: int = 0
        self.nyquist_zone_index: int = 0
        self.t_adc: int = 0
        self.fsub_type: int = 0
        self.payload_error: int = 0
        self.beam_repositioning_flag: int = 0
        # self.source_info 5-8 are reserved
        self.gn_index: int = 0
        self.marker_raw: bytes = bytes()
        self.station_info: int = 0
        self.observation_id: int = 0

        self.packet = packet

        self.unpack()
        self.unpack_station_info()
        self.unpack_source_info()

        # Only parse valid statistics packets from SDP, reject everything else
        if self.marker_raw not in self.valid_markers():
            raise ValueError(
                f"Invalid packet of type {self.__class__.__name__}: packet marker"
                f"(first byte) is {self.marker}, "
                f"not one of {self.valid_markers()}."
            )

    def __str__(self):
        return (
            f"_BasePacketHeader(marker={self.marker}, "
            f"station={self.station_id}, field={self.antenna_field_name}, "
            f"gn_index={self.gn_index}, "
            f"timestamp={self.timestamp.strftime('%FT%T')}, "
            f"payload_error={self.payload_error})"
        )

    @property
    def raw(self) -> bytes:
        """Returns the raw packet header bytes"""
        return self.packet

    @abstractmethod
    def unpack(self):
        """Unpack the packet bytes struct"""

    @abstractmethod
    def valid_markers(self):
        """Returns the valid packet markers"""

    def unpack_station_info(self):
        """Unpack the station_info field into properties of this object."""

        self.station_id = get_bit_value(self.station_info, 0, 9)
        self.antenna_field_index = get_bit_value(self.station_info, 10, 15)

    def unpack_source_info(self):
        """Unpack the source_info field into properties of this object."""

        self.antenna_band_index = get_bit_value(self.source_info, 15)
        self.nyquist_zone_index = get_bit_value(self.source_info, 13, 14)
        self.t_adc = get_bit_value(self.source_info, 12)
        self.fsub_type = get_bit_value(self.source_info, 11)
        self.payload_error = get_bit_value(self.source_info, 10) != 0
        self.beam_repositioning_flag = get_bit_value(self.source_info, 9) != 0
        # self.source_info 5-8 are reserved
        self.gn_index = get_bit_value(self.source_info, 0, 4)

    @abstractmethod
    def expected_size(self) -> int:
        """Determine packet size (header + payload) from the header"""

        # the generic header does not contain enough information to determine the
        # payload size
        raise NotImplementedError

    def size(self) -> int:
        """The actual size of this packet."""

        return len(self.packet)

    @property
    def marker(self) -> str:
        """Return the type of statistic:

        - 'S' = SST
        - 'B' = BST
        - 'X' = XST
        - 'b' = beamlet
        """

        try:
            return self.marker_raw.decode("ascii")
        except UnicodeDecodeError:
            # non-ascii (>127) character, return as binary
            #
            # this is typically not visible to the user, as these packets are not SDP
            # statistics packets, which the constructor will refuse to accept.
            return str()

    @property
    def antenna_field_name(self) -> str:
        """Returns the name of the antenna field as one of LBA-#0, HBA-#0, HBA-#1."""

        antenna_band = ["LBA", "HBA"][self.antenna_band_index]

        # NB: This returns HBA-#0 for both HBA0 and HBA
        return f"{antenna_band}-#{self.antenna_field_index}"

    @property
    def block_period(self) -> float:
        """Return the block period, in seconds."""

        return self.block_period_raw / 1e9

    @property
    def timestamp(self) -> datetime:
        """Returns the timestamp of the data in this packet.

        :return: datetime from block_serial_number and block period but
                 datetime.min if the block_serial_number in the packet is not set
                 datetime.max if the timestamp cannot be represented in python
        """

        try:
            return datetime.fromtimestamp(
                self.block_serial_number * self.block_period, timezone.utc
            )
        except ValueError:
            # Let's not barf anytime we want to print a header
            return datetime.max

    def __iter__(self):
        """Return all the header fields as a dict."""
        yield "marker", self.marker
        yield "marker_raw", self.marker_raw[0]
        yield "version_id", self.version_id
        yield "observation_id", self.observation_id
        yield "station_id", self.station_id
        yield "station_info", {
            "_raw": self.station_info,
            "station_id": self.station_id,
            "antenna_field_index": self.antenna_field_index,
        }

        yield "source_info", {
            "_raw": self.source_info,
            "antenna_band_index": self.antenna_band_index,
            "nyquist_zone_index": self.nyquist_zone_index,
            "t_adc": self.t_adc,
            "fsub_type": self.fsub_type,
            "payload_error": self.payload_error,
            "beam_repositioning_flag": self.beam_repositioning_flag,
            "gn_index": self.gn_index,
        }
        yield "block_period_raw", self.block_period_raw
        yield "block_period", self.block_period
        yield "block_serial_number", self.block_serial_number
        yield "timestamp", self.timestamp

        if self.t_adc == 0:
            yield "f_adc", 160
        elif self.t_adc == 1:
            yield "f_adc", 200
