#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""
Internal class for station states to observations
"""

import concurrent.futures
import logging
from json import dumps
from typing import List

from lofar_station_client._utils import sys_exit_no_tango
from lofar_station_client.observation.constants import (
    OBSERVATION_CONTROL_DEVICE_NAME,
    OBSERVATION_FIELD_DEVICE_NAME,
)

try:
    from tango import DeviceProxy, GreenMode, DevFailed
except ImportError:
    sys_exit_no_tango()

logger = logging.getLogger()


class StationObservationFuture:
    """
    Container class to manage an observation on a specific station using Python Futures
    Use :py:class:`StationObservation` as wrapper for synchronized blocking interface.
    """

    def __init__(self, specification: dict, host: str):
        """Construct object to manage observation on a station using Futures

        :param specification: Specification from TMSS (LOBSTER) should be of pattern:
            ``{
               "station": "cs002"
               "antenna_fields": [
                   {
                      "observation_id": 144345,
                      "antenna_field": "HBA0",
                      "antenna_set": "all",
                   }
                }
            }``
        :param host: Tango database host including port
        """
        # pylint: disable=C0103

        # Make no attempt to check each field has the same id
        self._host: str = host
        self._specification: dict = specification
        self._id = specification["antenna_fields"][0]["observation_id"]
        self._observation_field_proxies = None

        self._antenna_fields = []
        for observation_field in specification["antenna_fields"]:
            self._antenna_fields.append(observation_field["antenna_field"])

        try:
            # connects to the tangoDb and get the proxy
            self._control_proxy = DeviceProxy(
                f"tango://{self.host}/{OBSERVATION_CONTROL_DEVICE_NAME}"
            )
            # Increase timeout
            self._control_proxy.set_timeout_millis(10000)
            # gives an exception when it fails to ping the proxy
            _ = self._control_proxy.ping()

            # set station to green mode
            self._control_proxy.set_green_mode(GreenMode.Futures)

        except DevFailed as e:
            self._control_proxy = None

            logger.warning(
                "Failed to connect to device on host %s: %s: %s",
                host,
                e.__class__.__name__,
                e,
            )

    @staticmethod
    def _failed_future():
        future = concurrent.futures.Future()
        future.set_exception(Exception("Station not connected"))
        return future

    def start(self) -> concurrent.futures:
        """Start the observation with the given specification on this station"""
        if not self.connected:
            return self._failed_future()

        return self._control_proxy.add_observation(
            dumps(self._specification), wait=False
        )

    def stop(self) -> concurrent.futures:
        """Stop the observation with the given ID on this station"""
        if not self.connected:
            return self._failed_future()

        return self._control_proxy.stop_observation_now(self._id, wait=False)

    @property
    def is_running(self) -> concurrent.futures:
        """get whether the observation with the given ID is running on this station"""
        if not self.connected:
            return self._failed_future()

        return self._control_proxy.is_observation_running(self._id, wait=False)

    @property
    def connected(self) -> bool:
        """get the connection status of this station"""
        # TODO(Corne): What if connection fails between construction of object and call?
        return self._control_proxy is not None

    @property
    def host(self) -> str:
        """get the host name of this station"""
        return self._host

    @property
    def control_proxy(self) -> DeviceProxy:
        """get the control proxy of this station"""
        return self._control_proxy

    @property
    def observation_field_proxies(self) -> List[DeviceProxy]:
        """get the observationfield proxies of this station for the given observation"""

        if not self._observation_field_proxies:
            self._observation_field_proxies = []
            try:
                for observation_field in self._specification["antenna_fields"]:
                    antenna_field = observation_field["antenna_field"]
                    self._antenna_fields.append(antenna_field)
                    field_proxy = DeviceProxy(
                        f"tango://{self.host}/{OBSERVATION_FIELD_DEVICE_NAME}/"
                        f"{self._id}-{antenna_field}"
                    )
                    # Increase timeout
                    field_proxy.set_timeout_millis(10000)
                    self._observation_field_proxies.append(field_proxy)
            except DevFailed as ex:
                self._observation_field_proxies = None
                raise ex

        return self._observation_field_proxies
