#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from tempfile import TemporaryDirectory
from typing import List, Dict

import numpy
from numpy import ndarray, array

from lofar_station_client.file_access import (
    member,
    attribute,
    create_hdf5,
    read_hdf5,
    open_hdf5,
)
from tests import base


class SimpleSet:
    values: ndarray = member()


class DataSubSet:
    values: List[int] = member()
    dict_test_ndarray: Dict[str, ndarray] = member()
    dict_test_object: Dict[str, SimpleSet] = member()


class DataSet:
    observation_station: str = attribute()
    observation_source: str = attribute(from_member="sub_set")
    nof_payload_errors: List[int] = member()
    values: List[List[float]] = member()
    sub_set: DataSubSet = member(name="test")
    non_existent: DataSubSet = member(optional=True)


class SubArray(ndarray):
    observation_station: str = attribute()


class SubArrayDerived(SubArray):
    observation_source: str = attribute()


class SubDict(Dict[str, ndarray]):
    station_name: str = attribute()
    station_version: str = attribute()


class TestHdf5FileWriter(base.TestCase):
    def test_simple_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_simple_writing.h5"

            with create_hdf5(file_name, DataSet) as ds:
                ds.observation_station = "CS001"
                ds.nof_payload_errors = [1, 2, 3, 4, 5, 6]
                ds.values = [[2.0], [3.0], [4.0]]
                ds.sub_set = DataSubSet()
                ds.sub_set.values = [5, 4, 3, 2]
                ds.observation_source = "CasA"

            with read_hdf5(file_name, DataSet) as ds:
                self.assertEqual("CS001", ds.observation_station)
                self.assertEqual([1, 2, 3, 4, 5, 6], ds.nof_payload_errors)
                self.assertEqual([[2.0], [3.0], [4.0]], ds.values)
                self.assertIsNotNone(ds.sub_set)
                self.assertEqual([5, 4, 3, 2], ds.sub_set.values)
                self.assertEqual("CasA", ds.observation_source)

    def test_list_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_list_writing.h5"

            with create_hdf5(file_name, DataSubSet) as dss:
                dss.values = [2, 3, 4, 5]
                dss.values.append(1)

            with read_hdf5(file_name, DataSubSet) as dss:
                self.assertEqual([2, 3, 4, 5, 1], dss.values)

    def test_dict_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_dict_writing.h5"

            with create_hdf5(file_name, Dict[str, ndarray]) as d:
                d["test_1"] = array([1, 2, 3, 4, 5, 6])
                d["test_2"] = array([6, 5, 4, 1])

            with read_hdf5(file_name, Dict[str, ndarray]) as d:
                self.assertFalse(([1, 2, 3, 4, 5, 6] - d["test_1"]).any())
                self.assertFalse(([6, 5, 4, 1] - d["test_2"]).any())

    def test_derived_dict_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_derived_dict_writing.h5"

            with create_hdf5(file_name, SubDict) as d:
                d.station_name = "st1"
                d.station_version = "999"
                d["test_1"] = array([1, 2, 3, 4, 5, 6])
                d["test_2"] = array([6, 5, 4, 1])

            with read_hdf5(file_name, SubDict) as d:
                self.assertEqual("st1", d.station_name)
                self.assertEqual("999", d.station_version)
                self.assertFalse(([1, 2, 3, 4, 5, 6] - d["test_1"]).any())
                self.assertFalse(([6, 5, 4, 1] - d["test_2"]).any())

    def test_derived_ndarray_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_derived_ndarray_writing.h5"

            with create_hdf5(file_name, Dict[str, SubArray]) as d:
                sa = numpy.zeros((8,), dtype=numpy.float64).view(SubArray)
                sa.observation_station = "test1"
                d["test_1"] = sa
                sa2 = numpy.zeros((10,), dtype=numpy.float64).view(SubArray)
                sa2.observation_station = "test2"
                d["test_2"] = sa2

            with read_hdf5(file_name, Dict[str, SubArray]) as dss:
                self.assertTrue("test_1" in dss)
                self.assertTrue("test_2" in dss)
                self.assertEqual("test1", dss["test_1"].observation_station)
                self.assertEqual("test2", dss["test_2"].observation_station)

    def test_doubly_derived_ndarray_writing(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_doubly_derived_ndarray_writing.h5"

            with create_hdf5(file_name, Dict[str, SubArrayDerived]) as d:
                sa = numpy.zeros((8,), dtype=numpy.float64).view(SubArrayDerived)
                sa.observation_station = "test1"
                sa.observation_source = "source1"
                d["test_1"] = sa

            with read_hdf5(file_name, Dict[str, SubArrayDerived]) as dss:
                self.assertTrue("test_1" in dss)
                self.assertEqual("test1", dss["test_1"].observation_station)
                self.assertEqual("source1", dss["test_1"].observation_source)

    def test_dict_altering(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_dict_altering.h5"

            with create_hdf5(file_name, DataSubSet) as dss:
                dss.dict_test_ndarray = {
                    "test_1": array([2, 4, 6]),
                    "test_2": array([1, 3, 5]),
                }
                dss.dict_test_ndarray["test_3"] = array([9, 8, 7])
                dss.dict_test_ndarray.pop("test_1")
                ss = SimpleSet()
                ss.values = array([4, 9, 3])
                dss.dict_test_object = {"test_99": ss}
                dss.dict_test_object["test_99"].values[0] = 5
                dss.dict_test_object["test_98"] = SimpleSet()
                dss.dict_test_object["test_98"].values = array([4, 9, 3])

            with read_hdf5(file_name, DataSubSet) as dss:
                self.assertTrue("test_2" in dss.dict_test_ndarray)
                self.assertTrue("test_3" in dss.dict_test_ndarray)
                self.assertFalse(([1, 3, 5] - dss.dict_test_ndarray["test_2"]).any())
                self.assertFalse(([9, 8, 7] - dss.dict_test_ndarray["test_3"]).any())
                self.assertTrue("test_99" in dss.dict_test_object)
                self.assertTrue("test_98" in dss.dict_test_object)
                self.assertFalse(
                    ([5, 9, 3] - dss.dict_test_object["test_99"].values).any()
                )
                self.assertFalse(
                    ([4, 9, 3] - dss.dict_test_object["test_98"].values).any()
                )

    def test_object_access(self):
        ds = DataSet()
        ds.observation_station = "CS001"
        ds.nof_payload_errors = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ds.values = [[1.0]]
        ds.sub_set = DataSubSet()
        ds.sub_set.values = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ds.observation_source = "CasA"

        self.assertEqual("CS001", ds.observation_station)
        self.assertEqual(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ds.nof_payload_errors
        )
        self.assertEqual([[1.0]], ds.values)
        self.assertIsNotNone(ds.sub_set)
        self.assertEqual(
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], ds.sub_set.values
        )
        self.assertEqual("CasA", ds.observation_source)

    def test_attach_object(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_attach_object.h5"

            with create_hdf5(file_name, DataSet) as ds:
                sub_set = DataSubSet()
                sub_set.values = [7, 4, 9, 2, 9]
                ds.sub_set = sub_set
                ds.observation_source = "CasA"

            with read_hdf5(file_name, DataSet) as ds:
                self.assertEqual([7, 4, 9, 2, 9], ds.sub_set.values)
                self.assertEqual("CasA", ds.observation_source)

    def test_open_write(self):
        with TemporaryDirectory() as tmpdir:
            file_name = tmpdir + "/test_open_write.h5"

            with create_hdf5(file_name, DataSet) as ds:
                ds.observation_station = "CS001"
                ds.nof_payload_errors = [1, 2, 3, 4, 5, 6]
                ds.values = [[2.0], [3.0], [4.0]]
                ds.sub_set = DataSubSet()
                ds.sub_set.values = [5, 4, 3, 2]
                ds.observation_source = "CasA"

            with open_hdf5(file_name, DataSet) as ds:
                ds.nof_payload_errors.append(7)
                ds.values.append([5.0])
                ds.observation_source = "ACAS"
                ds.sub_set.values = [1, 2, 3]

            with read_hdf5(file_name, DataSet) as ds:
                self.assertEqual("CS001", ds.observation_station)
                self.assertEqual([1, 2, 3, 4, 5, 6, 7], ds.nof_payload_errors)
                self.assertEqual([[2.0], [3.0], [4.0], [5.0]], ds.values)
                self.assertIsNotNone(ds.sub_set)
                self.assertEqual([1, 2, 3], ds.sub_set.values)
                self.assertEqual("ACAS", ds.observation_source)
