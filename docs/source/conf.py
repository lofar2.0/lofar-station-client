# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from lofar_station_client import __version__

# -- General configuration ----------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinxcontrib.apidoc",
    "sphinx_rtd_theme",
]

apidoc_module_dir = "../../lofar_station_client"
apidoc_output_dir = "source_documentation"
apidoc_excluded_paths = []
apidoc_separate_modules = True
apidoc_toc_file = False
# This should include private methods but does not work
# https://github.com/sphinx-contrib/apidoc/issues/14
apidoc_extra_args = ["--private"]

# The suffix of source filenames.
source_suffix = [".rst"]

# The master toctree document.
master_doc = "index"

# General information about the project.
project = "lofar-station-client"
copyright = "2022, ASTRON"

# openstackdocstheme options
repository_name = "lofar2.0/lofar-station-client"
bug_project = "none"
bug_tag = ""
html_last_updated_fmt = "%Y-%m-%d %H:%M"

# If true, '()' will be appended to :func: etc. cross-reference text.
add_function_parentheses = True

version = __version__

modindex_common_prefix = ["lofar-station-client."]

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
add_module_names = True

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = "sphinx"

# -- Options for HTML output --------------------------------------------------

# The theme to use for HTML and HTML Help pages.  Major themes that come with
# Sphinx are currently 'default' and 'sphinxdoc'.
# html_theme_path = ["."]
html_theme = "sphinx_rtd_theme"
html_static_path = ["static"]
html_css_files = [
    "css/custom.css",
]

# Output file base name for HTML help builder.
htmlhelp_basename = "%sdoc" % project

# Conf.py variables exported to sphinx rst files access using |NAME|
variables_to_export = [
    "project",
    "copyright",
    "version",
]

frozen_locals = dict(locals())
rst_epilog = "\n".join(
    map(
        lambda x: f".. |{x}| replace:: {frozen_locals[x]}",  # noqa: F821
        variables_to_export,
    )
)
# Pep is not able to determine that frozen_locals always exists so noqa
del frozen_locals
