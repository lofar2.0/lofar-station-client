# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Conversions between signal and node indexes"""

import datetime
from typing import Tuple

from deprecate import deprecated
import numpy as np


def signal_index_2_processing_node_index(signal_index) -> int:
    """Convert signal index to processing node index.

    Can be used to determine on which processing node the signal is processed.
    """
    return (signal_index % 192) // 12


def signal_index_2_processing_node_input_index(signal_index) -> int:
    """Convert signal index to processing node input index.

    Can be used to determine wich input to the processing node is used for the signal.
    """
    return (signal_index % 192) % 12


def signal_index_2_uniboard_index(signal_index) -> int:
    """Convert signal index to UniBoard index.

    Can be used to determine on which UniBoard the signal is processed.
    """
    return signal_index // 48


def signal_index_2_rcu_index(signal_index) -> int:
    """Convert signal index to RCU index.

    Can be used to determine which RCU is used to provide the signal.
    """
    return (signal_index // 6) * 2 + signal_index % 2


def signal_index_2_rcu_input_index(signal_index) -> int:
    """Convert signal index to RCU input index.

    Can be used to determine which input to the RCU is used for the signal.
    """
    return (signal_index // 2) % 3


def signal_index_2_aps_index(signal_index) -> int:
    """Convert signal index to APS index.

    Can be used to determine in which Antenna Processing Subrack this signal is
    processed.
    """
    return signal_index // 96


def signal_input_2_rcu_index_and_rcu_input_index(signal_index) -> Tuple[int, int]:
    """Given the gsi, get the RCU index and the RCU input index in one call."""

    return (
        signal_index_2_rcu_index(signal_index),
        signal_index_2_rcu_input_index(signal_index),
    )


# the inverse:
def rcu_index_2_signal_index(rcu_index):
    """Convert RCU index to signal indices."""
    return rcu_index_and_rcu_input_index_2_signal_index(rcu_index)


def rcu_index_and_rcu_input_index_2_signal_index(rcu_index, rcu_input_index=None):
    """Convert RCU index and RCU input index to signal index."""

    if rcu_input_index is None:
        rcu_input_index = np.array([0, 1, 2])

    return rcu_index + rcu_input_index * 2 + (rcu_index // 2) * 4


@deprecated(target=signal_index_2_rcu_input_index, deprecated_in="0.2", remove_in="0.5")
def gsi_2_rcu_input_index(gsi):
    """Replaced by signal_index_2_rcu_input_index

    :deprecated: Please use signal_index_2_rcu_input_index.
    """

    return signal_index_2_rcu_input_index(gsi)


@deprecated(target=signal_index_2_rcu_index, deprecated_in="0.2", remove_in="0.5")
def gsi_2_rcu_index(gsi):
    """Replaced by signal_index_2_rcu_index

    `lofar_station_control`

    :deprecated: Please use signal_index_2_rcu_index.
    """

    return signal_index_2_rcu_index(gsi)


@deprecated(
    target=signal_input_2_rcu_index_and_rcu_input_index,
    deprecated_in="0.2",
    remove_in="0.5",
)
def gsi_2_rcu_index_and_rcu_input_index(gsi):
    """Replaced by signal_input_2_rcu_index_and_rcu_input_index

    :deprecated: Please use signal_input_2_rcu_index_and_rcu_input_index.
    """

    return signal_input_2_rcu_index_and_rcu_input_index(gsi)


@deprecated(target=rcu_index_2_signal_index, deprecated_in="0.2", remove_in="0.5")
def rcu_index_2_gsi(rcu_index):
    """Replaced by rcu_index_2_signal_index

    :deprecated: Please use rcu_index_2_signal_index.
    """
    return rcu_index_2_signal_index(rcu_index)


@deprecated(
    target=rcu_index_and_rcu_input_index_2_signal_index,
    deprecated_in="0.2",
    remove_in="0.5",
)
def rcu_index_and_rcu_input_index_2_gsi(rcu_index, rcu_input_index=None):
    """Replaced by rcu_index_and_rcu_input_index_2_signal_index

    :deprecated: Please use rcu_index_and_rcu_input_index_2_signal_index.
    """
    return rcu_index_and_rcu_input_index_2_signal_index(rcu_index, rcu_input_index)


def get_timestamp(format_specifier=None):
    """Get the timestamp in standard format

    :param format_specifier: format specifier. iso format (default)
                             "filename": filename without spaces and special
                             characters, format: yyyymmddThhmmss

    :return: Current timestamp in requested format
    """
    timestamp = datetime.datetime.isoformat(datetime.datetime.now())
    if format_specifier == "filename":
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    return timestamp
