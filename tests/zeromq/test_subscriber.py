# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

import asyncio
from datetime import datetime
import logging
import time
from threading import Thread

from timeout_decorator import timeout_decorator

from lofar_station_client.zeromq.publisher import ZeroMQPublisher
from lofar_station_client.zeromq.subscriber import (
    ZeroMQSubscriber,
    AsyncZeroMQSubscriber,
)

from tests import base

logger = logging.getLogger()


class TestSubscriber(base.TestCase):

    DEFAULT_PUBLISH_ADDRESS = "tcp://*:6001"
    DEFAULT_SUBSCRIBE_ADDRESS = "tcp://127.0.0.1:6001"

    @staticmethod
    def wait_for_start(publisher: ZeroMQPublisher):
        """Spin until publisher is running"""
        while not publisher.is_running:
            logger.info("Waiting for publisher thread to start..")
            time.sleep(0.1)

    @staticmethod
    def wait_for_connect(subscriber: ZeroMQSubscriber):
        """Spin until subscriber is running"""
        while not subscriber.is_connected:
            logger.info("Waiting for subscriber thread to start..")
            time.sleep(0.1)

    @staticmethod
    def wait_for_disconnect(subscriber: ZeroMQSubscriber):
        """Spin until subscriber is running"""
        while subscriber.is_connected:
            logger.info("Waiting for subscriber thread to stop..")
            time.sleep(0.1)

    @timeout_decorator.timeout(5)
    def test_recv(self):
        """Test receiving a message"""
        t_msg = "test"
        t_topic = "topic"

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as publisher:
            self.wait_for_start(publisher)

            with ZeroMQSubscriber(
                self.DEFAULT_SUBSCRIBE_ADDRESS, [t_topic]
            ) as t_subscriber:
                self.wait_for_connect(t_subscriber)

                for _ in range(
                    0, 5
                ):  # check against accidental shutdown after first message
                    publisher.send(t_msg)
                    topic, timestamp, msg = t_subscriber.recv()
                    self.assertIsInstance(timestamp, datetime)
                    self.assertEqual(t_msg, msg.decode())

    @timeout_decorator.timeout(5)
    def test_connects_disconnects(self):
        """Test connect/disconnect information."""
        t_topic = "topic"

        with ZeroMQSubscriber(
            self.DEFAULT_SUBSCRIBE_ADDRESS, [t_topic]
        ) as t_subscriber:
            self.assertFalse(t_subscriber.is_connected)
            self.assertEqual(0, t_subscriber.nr_connects)
            self.assertEqual(0, t_subscriber.nr_disconnects)

            with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as publisher:
                self.wait_for_start(publisher)

                self.wait_for_connect(t_subscriber)

                self.assertTrue(t_subscriber.is_connected)
                self.assertEqual(1, t_subscriber.nr_connects)
                self.assertEqual(0, t_subscriber.nr_disconnects)

            self.wait_for_disconnect(t_subscriber)

            self.assertFalse(t_subscriber.is_connected)
            self.assertEqual(1, t_subscriber.nr_connects)
            self.assertEqual(1, t_subscriber.nr_disconnects)


class TestAsyncSubscriber(base.TestCase):

    DEFAULT_PUBLISH_ADDRESS = "tcp://*:6001"
    DEFAULT_SUBSCRIBE_ADDRESS = "tcp://127.0.0.1:6001"

    def setUp(self):
        def run_event_loop(loop):
            asyncio.set_event_loop(loop)
            loop.run_forever()

        self.event_loop = asyncio.new_event_loop()
        thread = Thread(target=run_event_loop, args=(self.event_loop,), daemon=True)
        thread.start()

    @staticmethod
    def wait_for_start(publisher: ZeroMQPublisher):
        """Spin until publisher is running"""
        while not publisher.is_running:
            logger.info("Waiting for publisher thread to start..")
            time.sleep(0.1)

    @staticmethod
    async def wait_for_connect(subscriber: ZeroMQSubscriber):
        """Spin until subscriber is running"""
        while not subscriber.is_connected:
            logger.info("Waiting for subscriber thread to start..")
            await asyncio.sleep(0.1)

    @staticmethod
    async def wait_for_disconnect(subscriber: ZeroMQSubscriber):
        """Spin until subscriber is running"""
        while subscriber.is_connected:
            logger.info("Waiting for subscriber thread to stop..")
            await asyncio.sleep(0.1)

    @timeout_decorator.timeout(5)
    def test_connects_disconnects(self):
        """Test connect/disconnect information."""
        t_topic = "topic"

        async def run_subscriber():
            async with AsyncZeroMQSubscriber(
                self.DEFAULT_SUBSCRIBE_ADDRESS, [t_topic]
            ) as t_subscriber:
                self.assertFalse(t_subscriber.is_connected)
                self.assertEqual(0, t_subscriber.nr_connects)
                self.assertEqual(0, t_subscriber.nr_disconnects)

                with ZeroMQPublisher(
                    self.DEFAULT_PUBLISH_ADDRESS, [t_topic]
                ) as publisher:
                    self.wait_for_start(publisher)

                    await self.wait_for_connect(t_subscriber)

                    self.assertTrue(t_subscriber.is_connected)
                    self.assertEqual(1, t_subscriber.nr_connects)
                    self.assertEqual(0, t_subscriber.nr_disconnects)

                await self.wait_for_disconnect(t_subscriber)

                self.assertFalse(t_subscriber.is_connected)
                self.assertEqual(1, t_subscriber.nr_connects)
                self.assertEqual(1, t_subscriber.nr_disconnects)

        future = asyncio.run_coroutine_threadsafe(run_subscriber(), self.event_loop)
        _ = future.result()

    @timeout_decorator.timeout(5)
    def test_async_recv(self):
        """Test receiving a message"""
        t_msg = "test"
        t_topic = "topic"

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as publisher:
            self.wait_for_start(publisher)

            async def run_subscriber():
                async with AsyncZeroMQSubscriber(
                    self.DEFAULT_SUBSCRIBE_ADDRESS, [t_topic]
                ) as t_subscriber:
                    await self.wait_for_connect(t_subscriber)

                    for _ in range(
                        0, 5
                    ):  # check against accidental shutdown after first message
                        publisher.send(t_msg)
                        topic, timestamp, msg = await t_subscriber.async_recv()
                        self.assertIsInstance(timestamp, datetime)
                        self.assertEqual(t_msg, msg.decode())

            future = asyncio.run_coroutine_threadsafe(run_subscriber(), self.event_loop)
            _ = future.result()
