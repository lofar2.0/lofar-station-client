# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

# TODO(Corne): Fix paths in a way that both support tox debug & integration job
import base


class PrometheusTest(base.TestCase):
    def test_get_attribute_history_exported(self):
        from lofar_station_client import get_attribute_history

        self.assertIsNotNone(get_attribute_history)
