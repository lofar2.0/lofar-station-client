# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Constants used by DTS"""

# pylint: disable=consider-using-f-string, invalid-name

# number of polarisations per antenna (X and y polarisations)
N_pol = 2

# maximum number of fpgas supported by SDP
N_pn = 16

# antennas per FPGA
A_pn = 6

# signal inputs per FPGA
S_pn = A_pn * N_pol

RCU2L = [0, 1, 2, 3, 4, 5]
RCU2H = [8, 9]
RCU2HPWR = [8]  # Should be even numbers
# TODO(Corne): Why is this not an even number if it should be?!
RCU2HCTL = [9]  # Should be even numbers

CST_N_SUB = 512  # number of subbands as output of subband generation in firmware
CST_FS = 100e6  # sampling frequency in Hz
BANDLIMS = {"LB": [0, 100e6], "HB1": [100e6, 200e6], "HB2": [200e6, 300e6]}
LB_ALIASES = ["LB1", "LB2"]

# define some constants for the setup:
N_FPGA = 4  # number of fpgas

APS_LOCATION_LABELS = ["Slot%02d" % nr for nr in range(32)]

# positions of RCU boards. (0,0) is bottom, left
APS_RCU_POS_X = (
    [11, 10, 9, 8, 7, 6]
    + [11, 10, 9, 8, 7] * 2
    + [4, 3, 2, 1, 0] * 2
    + [5, 4, 3, 2, 1, 0]
)
APS_RCU_POS_Y = [2] * 6 + [1] * 5 + [0] * 5 + [2] * 5 + [1] * 5 + [0] * 6
# positions of the other modules in the APS
APS_MOD_POS_X = [0, 1, 3, 4, 5]
APS_MOD_POS_Y = [0] * 5

# if the names are correctly provided by the RCU2s,
# then update plot_subband_statistics() (see annotation there)
# and remove these two lines:
# TODO(Corne): Improve mechanism for defining / undefining these variables
RCU2L_TAGS = ["8393812", "8416938", "8469028", "8386883", "8374523"]
RCU2H_TAGS = ["8514859", "8507272"]

PLOT_DIR = "inspection_plots"
STATION_NAME = "DTS-Outside"

RCU_INDICES = range(32)

# RCU Mask:
RCU2L_MASK = [rcu_nr in RCU2L for rcu_nr in range(32)]

# LBA Masks
LBA_MASK = [[True] * 3 if rcu_nr in RCU2L else [False] * 3 for rcu_nr in range(32)]

# HBA masks
HBA_MASK = [[True] * 3 if rcui in RCU2H else [False] * 3 for rcui in RCU_INDICES]
HBA_PWR_MASK = [[True] * 3 if rcui in RCU2HPWR else [False] * 3 for rcui in RCU_INDICES]
HBA_CTL_MASK = [[True] * 3 if rcui in RCU2HCTL else [False] * 3 for rcui in RCU_INDICES]
