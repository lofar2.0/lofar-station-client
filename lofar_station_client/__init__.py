#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Control the code that controls us, the great eyes aimed at the sky!"""

from importlib import metadata

from lofar_station_client.requests.prometheus import PrometheusRequests

__version__ = metadata.version("lofar-station-client")

get_attribute_history = PrometheusRequests.get_attribute_history

__all__ = ["get_attribute_history"]
