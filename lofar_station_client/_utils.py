#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Provide package wide utils."""

import sys


def sys_exit_no_tango():
    """Exit with message that tango wasn't found"""
    sys.exit(
        "Error importing pytango, did you install the "
        "optional dependency 'station-client[tango]'"
    )
