# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import numpy as np

import copy

from unittest import mock

from lofar_station_client.dts import outside

from tests import base


class DTSOutsideTest(base.TestCase):
    def test_set_rcu_ant_masked_update_all(self):
        """Test masked mixing of new and old values, update all"""

        t_recv = mock.Mock()

        mask = [[True] * 3] * 12
        old = [[0] * 3] * 12
        new = [[1] * 3] * 12

        result = outside.set_rcu_ant_masked(t_recv, mask, copy.deepcopy(old), new)
        np.testing.assert_array_equal(result, new)

    def test_set_rcu_ant_masked_update_none(self):
        """Test masked mixing of new and old values, update none"""

        t_recv = mock.Mock()

        mask = [[False] * 3] * 12
        old = [[0] * 3] * 12
        new = [[1] * 3] * 12

        result = outside.set_rcu_ant_masked(t_recv, mask, copy.deepcopy(old), new)
        np.testing.assert_array_equal(result, old)

    def test_set_rcu_hba_mask_update_all(self):
        """Test masked mixing of new and old values, update all"""
        t_recv = mock.Mock()

        mask = [[True] * 3] * 12
        old = np.array([[0] * 3] * 12).flatten()
        new = np.array([[1] * 3] * 12).flatten()

        result = outside.set_rcu_hba_mask(t_recv, mask, copy.deepcopy(old), new)
        np.testing.assert_array_equal(result, new)

    def test_set_rcu_hba_mask_update_none(self):
        """Test masked mixing of new and old values, update none"""
        t_recv = mock.Mock()

        mask = [[False] * 3] * 12
        old = np.array([[0] * 3] * 12).flatten()
        new = np.array([[1] * 3] * 12).flatten()

        result = outside.set_rcu_hba_mask(t_recv, mask, copy.deepcopy(old), new)
        np.testing.assert_array_equal(result, old)
