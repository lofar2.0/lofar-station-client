#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""
Contains class for pythonic interface with observations
"""

import logging
import concurrent.futures

from lofar_station_client.observation.station_observation_future import (
    StationObservationFuture,
)

logger = logging.getLogger()


class MultiStationObservation:
    """
    This class provides a pythonic interface
    to start an observation on multiple stations.
    """

    TIMEOUT = 10

    def __init__(self, specifications: dict, hosts: list):
        """

        :param specifications: l2stationspecs as retrieved from TMSS in format:
            ``
            {
                "stations": [
                    {
                       "station": "cs002"
                       "antenna_fields": [
                           {
                              "observation_id": 144345,
                              "antenna_field": "HBA0",
                              "antenna_set": "all",
                           }
                        }
                     }
                ]
            }
            ``
        :param hosts: list of pytango database host:port strings
        """

        self._specifications = specifications

        if len(specifications["stations"]) != len(hosts):
            raise ValueError(
                "Should have a unique host per station in spec!, "
                f"{len(specifications['stations'])} != {len(hosts)}"
            )

        # get the ID, assumed to be unique across stations and antenna field
        self.observation_id = specifications["stations"][0]["antenna_fields"][0][
            "observation_id"
        ]

        # TODO(Corne): Check obs id across stations and antenna fields, enforce that it
        #              is the same id.

        # The list of _stations this class controls
        self._stations = []
        for index, specification in enumerate(self._specifications["stations"]):
            self._stations.append(StationObservationFuture(specification, hosts[index]))

    def _get_results(self, futures):
        # pylint: disable=W0703, C0103
        """
        This function takes the futures it is given and obtains the results.
        It checks whether the future got done in the first place or if it timed out.
        And it checks whether the result we got was an actual result or an exception.
        """

        # wait for the futures to complete in the given timeout
        done_set, _ = concurrent.futures.wait(
            futures, timeout=self.TIMEOUT, return_when=concurrent.futures.ALL_COMPLETED
        )

        results = []

        for future in futures:
            # if the future completed: check whether we got good results or an exception
            if future in done_set:
                try:
                    result = future.result()
                except Exception as e:
                    result = e
                results.append(result)
            else:
                # if we timed out, append a timeout error
                results.append(concurrent.futures.TimeoutError())

        return results

    @staticmethod
    def _check_success(results):
        """
        This function returns a bool list for all the command results
        it gives a True for every command that succeeded and a
        false for every command that failed
        """
        success_list = []

        # determine whether we were able to successfully start all observations
        for i in results:
            if isinstance(i, Exception):
                success_list.append(False)
            else:
                success_list.append(True)

        return success_list

    def start(self):
        """Start the observation or all hosts"""

        # send out the command to start the observation to all _stations at once
        commands = [station.start() for station in self._stations]

        # wait for the commands to get processed
        results = self._get_results(commands)

        if not all(self._check_success(results)):
            logger.warning("Warning: Unable to start one or more observations")

        # create the dict
        return {
            station.host: result for station, result in zip(self._stations, results)
        }

    def stop(self):
        """
        stops this observation in all connected stations
        """
        # send out the command to abort the observation to all _stations at once
        commands = [station.stop() for station in self._stations]

        # wait for the commands to get processed
        results = self._get_results(commands)

        if not all(self._check_success(results)):
            logger.warning("Warning: Unable to abort one or more observations")

        # create the dict
        return {
            station.host: result for station, result in zip(self._stations, results)
        }

    @property
    def observations(self) -> dict:
        """
        Returns a dict of host -> station for all the different hosts
        Value is None if the host is not connected
        """

        return {
            station.host: station if station.connected else None
            for station in self._stations
        }

    @property
    def is_running(self) -> dict:
        """
        Returns a dict whether the observation is running per host.
        Returns "{hostname}": False if the host is not connected or if
        the command returned an exception
        """

        # send out all the commands at once without waiting until each one is finished
        commands = [station.is_running for station in self._stations]

        # wait for the commands to get processed
        results = self._get_results(commands)

        # create the dict
        return {
            station.host: (
                result
                if station.connected and not isinstance(result, Exception)
                else False
            )
            for station, result in zip(self._stations, results)
        }

    @property
    def is_connected(self) -> dict:
        """
        Returns a dict for all hosts whether they are _connected or not.
        Returns "{hostname}": bool per station
        """
        return {station.host: station.connected for station in self._stations}

    @property
    def all_connected(self) -> bool:
        """
        Returns true if all stations are connected
        """
        return all(self.is_connected.values())
