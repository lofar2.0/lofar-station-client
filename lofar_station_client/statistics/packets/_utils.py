#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Module wide utils"""


def get_bit_value(value: int, first_bit: int, last_bit: int = None) -> int:
    """Return bits [first_bit:last_bit] from value as integer. Bit 0 = LSB.

    For example, extracting bits 2-3 from b'01100' returns 11 binary = 3 decimal:
        get_bit_value(b'01100', 2, 3) == 3

    If 'last_bit' is not given, just the value of bit 'first_bit' is returned.
    """

    # default last_bit to first_bit
    if last_bit is None:
        last_bit = first_bit

    return value >> first_bit & ((1 << (last_bit - first_bit + 1)) - 1)
