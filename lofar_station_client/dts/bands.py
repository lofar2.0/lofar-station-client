# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Band methods and conversions to and from frequencies"""

import copy
from typing import Any
from typing import Dict
from typing import List
from typing import Union
from typing import Tuple

import numpy as np
from nptyping import NDArray
from nptyping import String
from nptyping import Float

from lofar_station_client.dts import constants
from lofar_station_client.dts import index


def get_valid_band_names():
    """Return the valid band names."""

    return copy.deepcopy(constants.BANDLIMS)


def get_band_name_and_subband_index_for_frequency(
    freqs: Union[float, List[float]],
) -> Tuple[NDArray[Any, String], NDArray[Any, Float]]:
    """Get band name and subband index for one or more frequencies.

    :param freqs: one or more frequencies in Hz
    :return: list of band names (see get_valid_band_names)
             sbi = one or more indices of the subbands for which the frequencies
             should be returned
    """
    # make sure freqs is a list
    if not isinstance(freqs, list):
        freqs = [freqs]
    # get band name and subband index
    freqs = np.array(freqs)
    band_names = []
    for freq in freqs:
        band_name = get_band_name_for_frequency(freq)
        band_names.append(band_name)
    band_names = np.array(band_names)
    # get indices for band_name "LB"
    sbis = np.round(freqs * constants.CST_N_SUB / constants.CST_FS)
    # and update for the other bands:
    for idx in np.where(np.array(band_names) == "HB1")[0]:
        sbis[idx] = 2 * constants.CST_N_SUB - sbis[idx]
    for idx in np.where(np.array(band_names) == "HB2")[0]:
        sbis[idx] = sbis[idx] - 2 * constants.CST_N_SUB
    return band_names, sbis


def get_band_name_for_frequency(freq: float, bandlims: Dict[str, float] = None) -> str:
    """Get band name for frequency

    :param freq: one frequency in Hz
    :param bandlims: dict with band names and their limits (default: LOFAR2 bands)
    :return: name of analog band (see get_valid_band_names),
             empty string ("") if unsuccessful
    """
    if bandlims is None:
        bandlims = constants.BANDLIMS
    for band_name in bandlims:
        if np.min(bandlims[band_name]) <= freq <= np.max(bandlims[band_name]):
            return band_name
    return ""


def get_frequency_for_band_name(band_name: str):
    """Get all frequencies for given band name

    :param band_name: name of analog band (see get_valid_band_names)
    :return: frequencies in Hz for the selected band
    """

    return get_frequency_for_band_name_and_subband_index(band_name, sbi=np.arange(512))


def get_frequency_for_band_name_and_subband_index(
    band_name: str, sbi: Union[float, List[float], NDArray[Any, Float]]
) -> Union[float, List[float], NDArray[Any, Float]]:
    """Get frequency for band name and subband(s)

    Transparenently returns the same type as supplied for sbi parameter.

    :warning: Will return frequencies for non-existing subbands outside the
              range of :py:attr:`~constants.CST_N_SUB`

    :param band_name: Name of analog band (see get_valid_band_names)
    :param sbi: One or more indices of the subbands for which the frequencies
                should be returned
    :return: frequencies in Hz for the selected band and subband
    """

    # check input
    valid_band_names = get_valid_band_names()
    if band_name not in valid_band_names and band_name not in constants.LB_ALIASES:
        raise ValueError(
            f"Unknown band_name '{band_name}'. Valid band_names are: "
            f"{valid_band_names}"
        )

    # generate frequencies
    freqs = sbi * constants.CST_FS / constants.CST_N_SUB
    if band_name == "HB1":
        return 200e6 - freqs
    if band_name == "HB2":
        return 200e6 + freqs
    # else: # LB
    return freqs


# TODO(Bouwdewijn): dimensionality does not correspond to recv attributes
def get_band_names(
    rcu_band_select_r, rcu_pcb_version_r, lb_tags: List = None, hb_tags: List = None
):
    """Get the frequency band names per receiver, based on their band selection & RCU.

    PCB name is used to determine the band (Low Band "LB" or High Band "HB",
    None otherwise). The band selection is added as a postfix.
    RCU PCB version can also be used, but then the default tags will not be sufficient

    rcu_pcb_version_r can also hold IDs as returned by recv.RCU_PCB_ID_R
    In that case, the lb_tags and hb_tags should be passed on by the user

    :param rcu_band_select_r: As returned by recv.RCU_band_select_R
    :param rcu_pcb_version_r: As returned by recv.RCU_PCB_version_R
    :param lb_tags: Substrings in RCU_PCB_version_R fields to indicate Low Band
                    Default: `["RCU2L"]`
    :param hb_tags: Substrings in RCU_PCB_version_R fields to indicate High Band
                    Default: `["RCU2H"]`
    :return: list of strings indicating the band name per receiver.
             Returns `None` if no band name could be determined

    """

    # if IDs are passed on, convert to list of strings
    if isinstance(rcu_pcb_version_r, np.ndarray):
        rcu_pcb_version_r = [str(x) for x in rcu_pcb_version_r]
    #
    if lb_tags is None:
        lb_tags = ["RCU2L"]
    if hb_tags is None:
        hb_tags = ["RCU2H"]

    # This statement can never work if rcu_band_select_r is one dimensional
    n_signal_indices = rcu_band_select_r.shape[0] * rcu_band_select_r.shape[1]
    band_name_per_signal_index = [None] * n_signal_indices

    for rcu_index, _ in enumerate(rcu_band_select_r):
        for rcu_input_index, _ in enumerate(rcu_band_select_r[rcu_index]):
            signal_index = index.rcu_index_and_rcu_input_index_2_signal_index(
                rcu_index, rcu_input_index
            )

            this_band = None
            if _is_lowband(lb_tags, rcu_pcb_version_r[rcu_index]):
                this_band = f"LB{rcu_band_select_r[rcu_index][rcu_input_index]:1}"
            elif _is_highband(hb_tags, rcu_pcb_version_r[rcu_index]):
                this_band = f"HB{rcu_band_select_r[rcu_index][rcu_input_index]:1}"

            band_name_per_signal_index[signal_index] = this_band
    return band_name_per_signal_index


def _is_lowband(tags, indexes):
    for tag in tags:
        if tag not in indexes:
            continue
        return True
    return False


def _is_highband(tags, indexes):
    for tag in tags:
        if tag not in indexes:
            continue
        return True
    return False
