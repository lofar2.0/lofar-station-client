# TCP to HDF5 statistics writer

The TCP to HDF5 statistics writer can be started with `statistics_writer.py`
This script imports the receiver script and `HDF5Writer.py`. `receiver.py` only
takes care of receiving packets. `HDF5Writer.py` takes the receive function from
the receiver and uses it to obtain packets.  Any function that can deliver
statistics packets can be used by this code. `HDF5Writer.py` takes care of
processing the packets it receives, filling statistics matrices and writing
those matrices (as well as a bunch of metadata) to hdf5.


### TCP Statistics writer

The statistics writer can be called with the `statistics_writer.py` script.
This script can be called with the following arguments:
  ```
  -a  --host          the address to connect to
  -p  --port          the port to use (default: 0)
  -f  --file          file to read from (alternative to using host and port)
  -i  --interval      The time between creating new files in seconds (default: 3600)
  -o  --output_dir    specifies the folder to write all the files (default: current)
  -m  --mode          sets the statistics type to be decoded options: "SST", "XST", "BST" (default: SST)
  -v  --debug         takes no arguments, when used prints a lot of extra data to help with debugging (default: off)
  -d  --decimation    Configure the writer to only store one every n samples. Saves storage space (default: 1 (everything))
  -r  --reconnect     Set the writer to keep trying to reconnect whenever connection is lost. (default: off)
  -nt --no-tango      Disable connection to Tango device attribute values retrieval (default: false)
  -A  --antennafield  Device name for AntennaField Proxy device
  -S  --sdp           Device name for SDP Proxy device
  -TB --tilebeam      Device name for Tilebeam Proxy device
  -DB --digitalbeam   Device name for Digitalbeam Proxy device
  ```
An example call could be:

```l2ss-statistics-writer --host localhost --port 1234--mode XST --debug```

This starts the script up to listen on localhost:1234 for XSTs with debug mode
on.

## HFD5 structure
Statistics packets are collected by the StatisticsCollector in to a matrix. Once
the matrix is done or a newer timestamp arrives this matrix along with the
header of first packet header, nof_payload_errors and nof_valid_payloads. The
file will be named after the mode it is in and the timestamp of the statistics
packets. For example: `SST_1970-01-01-00-00-00.h5`.

```
File
|
|------ {mode_timestamp}  |- {statistics matrix}
|                         |- {first packet header}
|                         |- {nof_valid_payloads}
|                         |- {nof_payload_errors}
|
|------ {mode_timestamp}  |- {statistics matrix}
|                         |- {first packet header}
|                         |- {nof_valid_payloads}
|                         |- {nof_payload_errors}
|
...
```

### reader

There is a statistics reader that is capable of parsing multiple HDF5 statistics
files in to a more easily usable format. It also allows for filtering between
certain timestamps.

`statistics_reader.py` takes the following arguments:
`--files        list of files to parse`
`--end_time     highest timestamp to process in isoformat`
`--start_time   lowest timestamp to process in isoformat`

ex: `python3 statistics_reader.py --files SST_2021-10-04-07-36-52.h5 --end_time 2021-10-04#07:50:08.937+00:00`
This will parse all the statistics in the file `SST_2021-10-04-07-36-52.h5` up to the timestamp `2021-10-04#07:50:08.937+00:00`

This file can be used as both a testing tool and an example for dealing with HDF5 statistics.
The code serves can serve as a starting point for further development. To help with these purposes a bunch of simple
helper functions are provided.
