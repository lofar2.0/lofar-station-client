#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
from lofar_station_client.common import CaseInsensitiveString

from tests import base


class TestCaseInsensitiveString(base.TestCase):
    def test_a_in_b(self):
        """Get and set an item with different casing"""

        self.assertTrue(CaseInsensitiveString("hba0") in CaseInsensitiveString("HBA0"))

    def test_b_in_a(self):
        """Get and set an item with different casing"""

        self.assertTrue(CaseInsensitiveString("HBA0") in CaseInsensitiveString("hba0"))

    def test_a_not_in_b(self):
        """Get and set an item with different casing"""

        self.assertFalse(CaseInsensitiveString("hba0") in CaseInsensitiveString("LBA0"))
