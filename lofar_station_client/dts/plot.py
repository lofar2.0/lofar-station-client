# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Plotting functionality for DTS"""

# pylint: disable=R0914,C0209,R0915,R0913

import logging

import numpy as np
import matplotlib.pyplot as plt

from lofar_station_client.dts import bands
from lofar_station_client.dts import constants
from lofar_station_client.dts import common
from lofar_station_client.dts import index as plib

logger = logging.getLogger()


def print_fpga_input_statistics(devices):
    """Print FPGA input statistics"""
    devices = common.devices_list_2_dict(devices)
    sdp = devices["sdp"]

    # get data from device
    dc_offset = sdp.FPGA_signal_input_mean_R
    signal_input_rms = sdp.FPGA_signal_input_rms_R
    lock_to_adc = sdp.FPGA_jesd204b_csr_dev_syncn_R
    err0 = sdp.FPGA_jesd204b_rx_err0_R
    err1 = sdp.FPGA_jesd204b_rx_err1_R
    count = sdp.FPGA_jesd204b_csr_rbd_count_R
    signal_index = np.reshape(np.arange(count.shape[0] * count.shape[1]), count.shape)
    # get some info on the hardware connections
    pn_index = plib.signal_index_2_processing_node_index(signal_index)
    pn_input_index = plib.signal_index_2_processing_node_input_index(signal_index)
    rcu_index = plib.signal_index_2_rcu_index(signal_index)
    rcu_input_index = plib.signal_index_2_rcu_input_index(signal_index)

    # print
    print("+------------+-------------+----------------------------------------------+")
    print("|   Input    | Processed by|             Statistics of input              |")
    print("+---+---+----+------+------+---------+---------+-----+------+------+------+")
    print("|RCU|RCU| SI | Node | Node | DC      | RMS     | Lock| Err0 | Err1 | Count|")
    print("|idx|inp|    | Index| Input| Offset  |   (LSB) |     | (0x) | (0x) | (0x) |")
    print("|   |idx|    |      | Index|   (LSB) |         |     |      |      |      |")
    print("+---+---+----+------+------+---------+---------+-----+------+------+------+")
    pfmt = (
        "| %02d| %1d |%3d |  %02d  |  %2d  | %7.2f | %7.2f | %3s | %04x | %04x | %04x |"
    )
    for rcui, rcuii, signali, pni, pnii, dco, rms, lock, err_0, err_1, cnt in zip(
        rcu_index.flatten(),
        rcu_input_index.flatten(),
        signal_index.flatten(),
        pn_index.flatten(),
        pn_input_index.flatten(),
        dc_offset.flatten(),
        signal_input_rms.flatten(),
        lock_to_adc.flatten(),
        err0.flatten(),
        err1.flatten(),
        count.flatten(),
    ):
        print(
            pfmt % (rcui, rcuii, signali, pni, pnii, dco, rms, lock, err_0, err_1, cnt)
        )
    print("+---+---+----+------+------+---------+---------+-----+------+------+------+")


def report_setup_configuration(devices, save_fig=True):
    """Read name and version data from the setup and print that as a report"""
    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    apsct = devices["apsct"]
    apspu = devices["apspu"]
    unb2 = devices["unb2"]
    sdp = devices["sdp"]
    # Receiver / High Band Antenna Tile

    # APS Hardware
    # APS / Receiver Unit2 side
    rcu2_pcb_ids = recv.RCU_PCB_ID_R.tolist()
    rcu2_pcb_nrs = list(recv.RCU_PCB_number_R)
    rcu2_pcb_vrs = list(recv.RCU_PCB_version_R)

    # APS / UniBoard2 side
    # TODO(Boudewijn): FIXME - shouldn't this be a list with multiple entries?
    # See also LOFAR2-10774 (APSCT)
    apsct_pcb_ids = [apsct.APSCT_PCB_ID_R]
    apsct_pcb_nrs = [apsct.APSCT_PCB_number_R]
    apsct_pcb_vrs = [apsct.APSCT_PCB_version_R]
    # TODO(Boudewijn): FIXME - shoudln't this be a list with multiple entries?
    # See also LOFAR2-11434 (APSPU)
    apspu_pcb_ids = [apspu.APSPU_PCB_ID_R]
    apspu_pcb_nrs = [apspu.APSPU_PCB_number_R]
    apspu_pcb_vrs = [apspu.APSPU_PCB_version_R]
    unb2_pcb_ids = unb2.UNB2_PCB_ID_R.tolist()
    unb2_pcb_nrs = list(unb2.UNB2_PCB_number_R)
    unb2_pcb_vrs = list(sdp.FPGA_hardware_version_R)
    # TODO(Boudewijn): FIXME:
    # FPGA_hardware_version_R returns hardware version info per FPGA. Hardware is shared
    # per 4 fpgas, so why repeating values here?
    unb2_pcb_vrs = [unb2_pcb_vrs[idx] for idx in [0, 4, 8, 12]]

    # TODO(Boudewijn): FIXME:
    # fixing list lengths for a large station with multiple APS
    # this makes sure that the list indexing below works
    n_aps = 4

    apsct_pcb_ids = apsct_pcb_ids + [0] * (n_aps - len(apsct_pcb_ids))
    apsct_pcb_nrs = apsct_pcb_nrs + [""] * (n_aps - len(apsct_pcb_nrs))
    apsct_pcb_vrs = apsct_pcb_vrs + [""] * (n_aps - len(apsct_pcb_vrs))

    apspu_pcb_ids = apspu_pcb_ids + [0] * (n_aps * 2 - len(apspu_pcb_ids))
    apspu_pcb_nrs = apspu_pcb_nrs + [""] * (n_aps * 2 - len(apspu_pcb_nrs))
    apspu_pcb_vrs = apspu_pcb_vrs + [""] * (n_aps * 2 - len(apspu_pcb_vrs))

    unb2_pcb_ids = unb2_pcb_ids + [0] * (n_aps * 2 - len(unb2_pcb_ids))
    unb2_pcb_nrs = unb2_pcb_nrs + [""] * (n_aps * 2 - len(unb2_pcb_nrs))
    unb2_pcb_vrs = unb2_pcb_vrs + [""] * (n_aps * 2 - len(unb2_pcb_vrs))

    logging.debug("APSPU:")
    logging.debug("apsct_pcb_ids: %s", apsct_pcb_ids)
    logging.debug("apsct_pcb_nrs: %s", apsct_pcb_nrs)
    logging.debug("apsct_pcb_vrs: %s", apsct_pcb_vrs)
    logging.debug("APSPU:")
    logging.debug("apspu_pcb_ids: %s", apspu_pcb_ids)
    logging.debug("apspu_pcb_nrs: %s", apspu_pcb_nrs)
    logging.debug("apspu_pcb_vrs: %s", apspu_pcb_vrs)
    logging.debug("UNB2:")
    logging.debug("unb2_pcb_ids: %s", unb2_pcb_ids)
    logging.debug("unb2_pcb_nrs: %s", unb2_pcb_nrs)
    logging.debug("unb2_pcb_vrs: %s", unb2_pcb_vrs)

    aps_pcb_ids = [
        apspu_pcb_ids[1],
        unb2_pcb_ids[0],
        apsct_pcb_ids[0],
        apspu_pcb_ids[0],
        unb2_pcb_ids[1],
    ]
    aps_pcb_nrs = [
        apspu_pcb_nrs[1],
        unb2_pcb_nrs[0],
        apsct_pcb_nrs[0],
        apspu_pcb_nrs[0],
        unb2_pcb_nrs[1],
    ]
    aps_pcb_vers = [
        apsct_pcb_vrs[1],
        unb2_pcb_vrs[0],
        apsct_pcb_vrs[0],
        apsct_pcb_vrs[0],
        unb2_pcb_vrs[1],
    ]

    if not save_fig:
        fig_filenames = []
    else:
        fig_filenames = [
            f'{constants.PLOT_DIR}/{plib.get_timestamp("filename")}_aps_config.png',
            f'{constants.PLOT_DIR}/{plib.get_timestamp("filename")}_aps_config.pdf',
        ]
    plot_aps_configuration(
        rcu2_pcb_ids,
        rcu2_pcb_nrs,
        rcu2_pcb_vrs,
        aps_pcb_ids,
        aps_pcb_nrs,
        aps_pcb_vers,
        filenames=fig_filenames,
    )

    # Services
    # Firmware images
    firmware_versions = list(sdp.FPGA_firmware_version_R)
    # make sure there are versions for at least n_aps subracks
    firmware_versions = firmware_versions + [""] * (n_aps * 8 - len(firmware_versions))
    aps_services = {}
    for apsi, fpgai in zip(sorted(list(range(n_aps)) * 8), range(n_aps * 8)):
        aps_services["aps%i-fpga%02d" % (apsi, fpgai)] = firmware_versions[fpgai]
        aps_services[f"aps{apsi}-unb2tr" % apsi] = ""  # TODO(Boudewijn):
        aps_services[f"aps{apsi}-recvtr" % apsi] = ""  # TODO(Boudewijn):
        aps_services[f"aps{apsi}-apscttr" % apsi] = ""  # TODO(Boudewijn):
        aps_services[f"aps{apsi}-apsputr" % apsi] = ""  # TODO(Boudewijn):
    # Translators
    sdptr_software_versions = [sdp.TR_software_version_R]
    sdptr_software_versions = sdptr_software_versions + [""] * (
        2 - len(sdptr_software_versions)
    )
    station_services = {}
    station_services["sdptr0"] = sdptr_software_versions[0]
    station_services["sdptr1"] = sdptr_software_versions[1]
    station_services["ccdtr"] = ""  # TODO(Boudewijn): currently missing
    # station control tango devices
    for dev in devices:
        station_services[devices[dev].name().lower()] = devices[dev].version_R
    if not save_fig:
        figs = []
    else:
        figs = [
            f"{constants.PLOT_DIR}/{plib.get_timestamp('filename')}"
            "_station_services.png",
            f"{constants.PLOT_DIR}/{plib.get_timestamp('filename')}"
            "_station_services.pdf",
        ]
    plot_services_configuration(aps_services, station_services, filenames=figs)


def plot_aps_configuration(
    rcu_pcb_identifiers,
    rcu_pcb_numbers,
    rcu_pcb_versions,
    aps_pcb_ids,
    aps_pcb_nrs,
    aps_pcb_vers,
    filenames=None,
):  # pylint: disable=too-many-positional-arguments
    """Plot the configuration of the Antenna Processing Subrack, both sides."""

    fig, axs = plt.subplots(1, 2, figsize=(20, 8))

    if filenames is None:
        filenames = [
            f"{constants.PLOT_DIR}/aps_config.png",
            f"{constants.PLOT_DIR}/aps_config.pdf",
        ]

    plot_aps_rcu2_configuration(
        axs[0], rcu_pcb_identifiers, rcu_pcb_numbers, rcu_pcb_versions
    )
    plot_aps_unb2_configuration(axs[1], aps_pcb_ids, aps_pcb_nrs, aps_pcb_vers)
    for filename in filenames:
        fig.savefig(filename)
    fig.show()


def plot_aps_rcu2_configuration(
    plot_ax,
    rcu_pcb_identifiers,
    rcu_pcb_numbers,
    rcu_pcb_versions,
    rcu_locations=constants.APS_LOCATION_LABELS,
    rcu_pos_x=None,
    rcu_pos_y=None,
):  # pylint: disable=too-many-positional-arguments
    """Plot the configuration of the Antenna Processing Subrack, RCU2 side."""

    if rcu_pos_x is None:
        rcu_pos_x = constants.APS_RCU_POS_X
    if rcu_pos_y is None:
        rcu_pos_y = constants.APS_RCU_POS_Y
    plot_ax.plot(0, 0)
    plot_dx = plot_dy = 0.9
    plot_ax.set_xlim(-1 + plot_dx, np.max(rcu_pos_x) + 1)
    plot_ax.set_ylim(-1 + plot_dy, np.max(rcu_pos_y) + 1)

    for plot_x, plot_y, loc, pcb_id, pcb_nr, pcb_v in zip(
        rcu_pos_x,
        rcu_pos_y,
        rcu_locations,
        rcu_pcb_identifiers,
        rcu_pcb_numbers,
        rcu_pcb_versions,
    ):
        plot_ax.plot(
            [plot_x, plot_x + plot_dx, plot_x + plot_dx, plot_x, plot_x],
            [plot_y, plot_y, plot_y + plot_dy, plot_y + plot_dy, plot_y],
            color="black",
            alpha=0.2,
        )
        plot_ax.text(plot_x + plot_dx / 2, plot_y + plot_dy, loc, va="top", ha="center")
        plot_ax.text(
            plot_x + plot_dx / 2,
            plot_y + plot_dy / 2,
            "%s\n%s\n%s" % (pcb_v, pcb_id, pcb_nr),
            va="center",
            ha="center",
            rotation=90,
        )
        if pcb_id == 0 and pcb_v == "" and pcb_nr == "":
            plot_ax.plot(
                [plot_x, plot_x + plot_dx],
                [plot_y, plot_y + plot_dy],
                color="black",
                alpha=0.2,
            )
            plot_ax.plot(
                [plot_x + plot_dx, plot_x],
                [plot_y, plot_y + plot_dy],
                color="black",
                alpha=0.2,
            )
    plot_ax.set_title("Configuration of RCU2s")
    plot_ax.set_xticks([])
    plot_ax.set_yticks([])


def plot_aps_unb2_configuration(
    plot_ax,
    aps_pcb_identifiers,
    aps_pcb_numbers,
    aps_pcb_versions,
    mod_locations=constants.APS_LOCATION_LABELS,
    mod_pos_x=None,
    mod_pos_y=None,
):  # pylint: disable=too-many-positional-arguments
    """Plot the configuration of the Antenna Processing Subrack, UNB2 side."""

    if mod_pos_x is None:
        mod_pos_x = constants.APS_MOD_POS_X
    if mod_pos_y is None:
        mod_pos_y = constants.APS_MOD_POS_Y
    plot_ax.plot(0, 0)
    plot_dx = 0.9
    plot_dy = 3.9
    plot_ax.set_xlim(-1 + plot_dx, np.max(mod_pos_x) + 2)
    plot_ax.set_ylim(-1 + plot_dx, np.max(mod_pos_y) + 4)

    for plot_x, plot_y, loc, pcb_id, pcb_nr, pcb_v in zip(
        mod_pos_x,
        mod_pos_y,
        mod_locations,
        aps_pcb_identifiers,
        aps_pcb_numbers,
        aps_pcb_versions,
    ):
        plot_ax.plot(
            [plot_x, plot_x + plot_dx, plot_x + plot_dx, plot_x, plot_x],
            [plot_y, plot_y, plot_y + plot_dy, plot_y + plot_dy, plot_y],
            color="black",
            alpha=0.2,
        )
        plot_ax.text(plot_x + plot_dx / 2, plot_y + plot_dy, loc, va="top", ha="center")
        plot_ax.text(
            plot_x + plot_dx / 2,
            plot_y + plot_dy / 2,
            "%s\n%s\n%s" % (pcb_v, pcb_id, pcb_nr),
            va="center",
            ha="center",
            rotation=90,
        )
        if pcb_id == 0 and pcb_v == "" and pcb_nr == "":
            plot_ax.plot(
                [plot_x, plot_x + plot_dx],
                [plot_y, plot_y + plot_dy],
                color="black",
                alpha=0.2,
            )
            plot_ax.plot(
                [plot_x + plot_dx, plot_x],
                [plot_y, plot_y + plot_dy],
                color="black",
                alpha=0.2,
            )
    plot_ax.set_title("Configuration of UniBoard2 side")
    plot_ax.set_xticks([])
    plot_ax.set_yticks([])


def plot_services_configuration(
    aps_services,
    station_services,
    filenames=None,
):
    """Plot the service version information in the setup."""

    if filenames is None:
        filenames = [
            f"{constants.PLOT_DIR}/station_services.png",
            f"{constants.PLOT_DIR}/station_services.pdf",
        ]

    fig, plot_ax = plt.subplots(1, 1, figsize=(16, 15))

    aps_service_dx = 0.9
    aps_service_dy = 0.9
    first_aps_service = sorted(aps_services)[0][5:]
    plot_y = plot_y0 = len(aps_services) / 2 + 2
    for aps_service in sorted(aps_services):
        plot_x = 0 if (aps_service[3] in "01") else 1
        plot_y = (
            plot_y0
            if (aps_service[5:] == first_aps_service and aps_service[3] in "02")
            else plot_y - 1
        )
        plot_y = (
            plot_y - 1
            if (aps_service[5:] == first_aps_service and aps_service[3] in "13")
            else plot_y
        )
        plot_ax.plot(
            [plot_x, plot_x + aps_service_dx, plot_x + aps_service_dx, plot_x, plot_x],
            [plot_y, plot_y, plot_y + aps_service_dy, plot_y + aps_service_dy, plot_y],
            color="black",
            alpha=0.2,
        )
        plot_ax.text(
            plot_x,
            plot_y + aps_service_dy / 2,
            aps_service,
            va="center",
            ha="left",
            color="black",
        )
        plot_ax.text(
            plot_x + aps_service_dx,
            plot_y + aps_service_dy / 2,
            aps_services[aps_service],
            va="center",
            ha="right",
            color="blue",
        )
        if aps_services[aps_service] == "":
            plot_ax.plot(
                [plot_x, plot_x + aps_service_dx],
                [plot_y, plot_y + aps_service_dy],
                color="black",
                alpha=0.2,
            )
            plot_ax.plot(
                [plot_x + aps_service_dx, plot_x],
                [plot_y, plot_y + aps_service_dy],
                color="black",
                alpha=0.2,
            )

    stat_service_dx = 1.9
    stat_service_dy = 0.9
    plot_x = 0
    plot_y = 0
    for stat_service in sorted(station_services):
        plot_ax.plot(
            [
                plot_x,
                plot_x + stat_service_dx,
                plot_x + stat_service_dx,
                plot_x,
                plot_x,
            ],
            [
                plot_y,
                plot_y,
                plot_y + stat_service_dy,
                plot_y + stat_service_dy,
                plot_y,
            ],
            color="black",
            alpha=0.2,
        )
        plot_ax.text(
            plot_x,
            plot_y + stat_service_dy / 2,
            stat_service,
            va="center",
            ha="left",
            color="black",
        )
        plot_ax.text(
            plot_x + stat_service_dx,
            plot_y + stat_service_dy / 2,
            station_services[stat_service],
            va="center",
            ha="right",
            color="blue",
        )
        if station_services[stat_service] == "":
            plot_ax.plot(
                [plot_x, plot_x + stat_service_dx],
                [plot_y, plot_y + stat_service_dy],
                color="black",
                alpha=0.2,
            )
            plot_ax.plot(
                [plot_x + stat_service_dx, plot_x],
                [plot_y, plot_y + stat_service_dy],
                color="black",
                alpha=0.2,
            )
        plot_y -= 1
    plot_ax.set_title("Services in setup")
    plot_ax.set_xticks([])
    plot_ax.set_yticks([])

    for filename in filenames:
        fig.savefig(filename)
    fig.show()


def plot_statistics(devices, save_fig=True):
    """Plot the statistics: SST, XST and BST"""

    logging.info("Plot subband statistics")
    if not save_fig:
        figs = []
    else:
        figs = [
            f"{constants.PLOT_DIR}/{plib.get_timestamp('filename')}"
            "_subband_statistics.png",
            f"{constants.PLOT_DIR}/{plib.get_timestamp('filename')}"
            "_subband_statistics.pdf",
        ]
    plot_subband_statistics(devices, filenames=figs)

    # TODO(Boudewijn):
    # if not silent:
    #     plib.log("Plot crosslet statistics")
    # TODO(Boudewijn):
    # if not silent:
    #     plib.log("Plot beamlet statistics")


def plot_subband_statistics(
    devices,
    xlim=None,
    ylim=None,
    filenames=None,
):
    """Make the subband statistics plot"""

    if filenames is None:
        filenames = [
            f"{constants.PLOT_DIR}/subband_statistics.png",
            f"{constants.PLOT_DIR}/subband_statistics.pdf",
        ]

    if xlim is None:
        xlim = [-5, 305]
    if ylim is None:
        ylim = [-105, -30]
    sst_data, band_names, frequency_axes = _get_subband_statistics(devices)

    fig, _ = plt.subplots(1, 1, figsize=(18, 6))
    for signal_index in range(len(sst_data)):
        if band_names[signal_index] is None:
            continue
        freq = frequency_axes[band_names[signal_index]]
        # plot data in dB full scale
        plot_data = 10 * np.log10(sst_data[signal_index, :] + 1) - 128 - 6 * 4
        # since we're lacking antenna names,
        # show the RCU input info as label
        rcu_index, rcu_input_index = plib.signal_input_2_rcu_index_and_rcu_input_index(
            signal_index
        )
        label = "RCU%02d, Inp%d" % (rcu_index, rcu_input_index)
        plt.plot(freq / 1e6, plot_data, label=label)
    plt.grid()
    plt.legend(bbox_to_anchor=(1, 1), loc="upper left")
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.xlabel("Frequency (MHz)")
    plt.ylabel("Power (dB full scale)")
    plt.suptitle("Subband Statistics for all fully-connected receivers")
    plt.title(f"{constants.STATION_NAME}; {plib.get_timestamp()}")

    for filename in filenames:
        fig.savefig(filename)
    fig.show()


def _get_subband_statistics(devices):
    """Get subband statistics data from the setup, including proper axes and labels"""

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    sst = devices["sst"]

    # get band names for the current configuration
    cur_band_names = bands.get_band_names(
        recv.RCU_band_select_R,
        # TODO(Boudewijn): when correct LCNs are provided
        # in "RCU_PCB_number_R", uncomment the
        # following line:
        # recv.RCU_PCB_number_R,
        # and remove the following line:
        recv.RCU_PCB_ID_R,
        lb_tags=constants.RCU2L_TAGS,
        hb_tags=constants.RCU2H_TAGS,
    )

    # replace the aliases of LB1 and LB2 into LB: (same frequency axis)
    for alias in constants.LB_ALIASES:
        while alias in cur_band_names:
            cur_band_names[cur_band_names.index(alias)] = "LB"
    # get frequency axes

    # TODO(Boudewijn): get frequency axes correct (indexing is incorrect/inconsequent)
    key1 = "HB2"
    key2 = "HB1"
    tmp_key = "magic_band"
    while key1 in cur_band_names:
        cur_band_names[cur_band_names.index(key1)] = tmp_key
    while key2 in cur_band_names:
        cur_band_names[cur_band_names.index(key1)] = key1
    while tmp_key in cur_band_names:
        cur_band_names[cur_band_names.index(tmp_key)] = key2

    frequency_axes = {}
    for band_name in bands.get_valid_band_names():
        if band_name in cur_band_names:
            frequency_axes[band_name] = bands.get_frequency_for_band_name(band_name)
    # get sst data
    sst_data = sst.sst_r
    # make sure header data is of correct size
    cur_band_names = cur_band_names + [None] * (len(sst_data) - len(cur_band_names))
    return sst_data, cur_band_names, frequency_axes
