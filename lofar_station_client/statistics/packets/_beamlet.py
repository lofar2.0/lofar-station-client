#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-many-instance-attributes,too-many-function-args,duplicate-code

"""Beamlet related package definitions"""

import struct

import numpy

from ._base import _BasePacketHeader
from ._utils import get_bit_value

N_POL = 2
N_COMPLEX = 2


class BeamletHeader(_BasePacketHeader):
    """Models a beamlet UDP packet from SDP.

    Packets are expected to be UDP payload only (so no Ethernet/IP/UDP headers).

    The following additional fields are exposed as properties & functions.

    beamlet_width:

        bits/beamlet (4, 8, 16).

    beamlet_scale:

        scaling of beamlets prior to quantisation.

    beamlet_index:

        index of the first beamlet in the payload.

    nof_blocks_per_packet:

        number of blocks (timestamps) of data in the payload.

    nof_beamlets_per_block:

        number of beamlets in the payload.
    """

    # format string for the header, see unpack below
    HEADER_FORMAT = ">cBL HBH xxxx HHBHHQ"
    HEADER_SIZE = struct.calcsize(HEADER_FORMAT)

    def __init__(self, packet: bytes):
        self.beamlet_width: int = 0
        self.beamlet_scale: int = 0
        self.beamlet_index: int = 0
        self.nof_blocks_per_packet: int = 0
        self.nof_beamlets_per_block: int = 0
        super().__init__(packet)

    def __str__(self):
        return (
            f"BeamletHeader("
            f"station={self.station_id}, field={self.antenna_field_name}, "
            f"gn_index={self.gn_index}, "
            f"timestamp={self.timestamp.strftime('%FT%T')}, "
            f"payload_error={self.payload_error})"
        )

    def unpack(self):
        """Unpack the packet into properties of this object."""

        # unpack fields
        try:
            (
                self.marker_raw,
                self.version_id,
                self.observation_id,
                self.station_info,
                source_info_h,
                source_info_l,
                self.beamlet_scale,
                self.beamlet_index,
                self.nof_blocks_per_packet,
                self.nof_beamlets_per_block,
                self.block_period_raw,
                self.block_serial_number,
            ) = struct.unpack(self.HEADER_FORMAT, self.packet[: self.HEADER_SIZE])
        except struct.error as ex:
            raise ValueError("Error parsing beamlet packet") from ex

        self.source_info = (source_info_h << 16) + source_info_l
        # unpack the fields we just updated
        self.unpack_station_info()
        self.unpack_source_info()

    def valid_markers(self):
        return [b"b"]

    def unpack_source_info(self):
        """Unpack the source_info field into properties of this object."""

        self.beamlet_width = get_bit_value(self.source_info, 8, 11) or 16

    def expected_size(self) -> int:
        """The size this packet should be (header + payload), extracted from header."""

        return (
            self.HEADER_SIZE
            + self.nof_statistics_per_packet * self.nof_bytes_per_statistic
        )

    @property
    def nof_statistics_per_packet(self) -> int:
        """Compute number of statistics per packet."""

        return (
            self.nof_blocks_per_packet
            * self.nof_beamlets_per_block
            * N_POL
            * N_COMPLEX
            * self.beamlet_width
            // 8
        )

    @property
    def nof_bytes_per_statistic(self) -> int:
        """Determine number of bytes per statistic."""

        if self.beamlet_width == 8:
            # cint8 data [-127, 127]
            return 1
        if self.beamlet_width == 16:
            # cint16 data [-32767, 32767]
            return 2
        if self.beamlet_width == 4:
            # cint4 data [-7, 7], packet in 1 byte
            return 1

        return -1

    def __iter__(self):
        """Return all the header fields as a dict."""
        for key, value in super().__iter__():
            if key == "source_info":
                yield key, {**value, "beamlet_width": self.beamlet_width}
            else:
                yield key, value
        yield "beamlet_scale", self.beamlet_scale
        yield "beamlet_index", self.beamlet_index
        yield "nof_blocks_per_packet", self.nof_blocks_per_packet
        yield "nof_beamlets_per_block", self.nof_beamlets_per_block


class BeamletPacket:
    """Models a beamlet UDP packet from SDP.

    Packets are expected to be UDP payload only (so no Ethernet/IP/UDP headers).

    """

    def __init__(self, header: BeamletHeader, payload: bytes):
        self._header: BeamletHeader = header
        self.payload_data: bytes = payload

    @staticmethod
    def parse_packet(packet_data: bytes) -> "BeamletPacket":
        """Parses bytes into a BeamletPacket"""
        header = BeamletHeader(packet_data[0 : BeamletHeader.HEADER_SIZE])
        return BeamletPacket(header, packet_data[BeamletHeader.HEADER_SIZE :])

    @property
    def raw(self) -> bytes:
        """Returns the raw packet bytes"""
        return self._header.raw + self.payload_data

    @property
    def header(self) -> BeamletHeader:
        """Returns the packet header"""
        return self._header

    def payload_raw(self):
        """Returns the payload as numpy array"""
        bytecount_to_unsigned_struct_type = {1: "b", 2: "h", 4: "i", 8: "q"}
        format_str = (
            f">{self.header.nof_statistics_per_packet}"
            f"{bytecount_to_unsigned_struct_type[self.header.nof_bytes_per_statistic]}"
        )

        payload = numpy.array(
            struct.unpack(
                format_str,
                self.payload_data,
            )
        )

        # Generate payload as linear array
        if self.header.beamlet_width == 4:
            payload.reshape(
                self.header.nof_blocks_per_packet,
                self.header.nof_beamlets_per_block,
                N_POL,
            )

        return (
            payload
            # TODO(Corne): Verify reshape can actually take > 2 arguments
            .reshape(
                self.header.nof_blocks_per_packet,
                self.header.nof_beamlets_per_block,
                N_POL,
                N_COMPLEX,
            )
        )

    def payload(self):
        """A property that should probably be removed"""

        if self.header.beamlet_width == 4:
            # real in low 4 bits, imag in high 4 bits (both signed!)
            payload_raw = self.payload_raw()

            # shift extends sign, so we prefer it over bitwise and
            return (payload_raw << 4 >> 4) + (payload_raw >> 4) * 1j

        return self.payload_raw().astype(float).view(numpy.complex64)
