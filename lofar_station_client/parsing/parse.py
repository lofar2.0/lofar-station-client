# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Type parsing and validation functionality"""

from typing import List
from typing import Tuple
from typing import Union


def validate_type(param, allowed_types: List[type]) -> bool:
    """Ensure that the param is of a type in allowed_types

    :return: True if of type in allowed_types, False otherwise
    """

    def bare_type_unless_none(arg: Union[type, None]) -> type:
        """Convert bare None to its NoneType as isinstance(None, None) returns False"""
        return arg if arg is not None else type(arg)

    for current_type in allowed_types:
        if isinstance(param, bare_type_unless_none(current_type)):
            return True

    return False


def validate_types(
    parameters: List[Tuple[any, List[any], str]], message: Union[str, None] = None
):
    """Validate function parameter types for user facing function.

    Pass as a list with [(argument, [type1, type2]. 'name of argument'), ...]

    :raises TypeError: For any argument that does not have a type in the specified list
    """

    if not message:
        message = "Argument {0} of type {1} must be one of {2}"

    for validate in parameters:
        if not validate_type(validate[0], validate[1]):
            raise TypeError(message.format(validate[2], type(validate[0]), validate[1]))
