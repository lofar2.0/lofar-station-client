# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import numpy as np

from lofar_station_client.dts import index

from tests import base


class DTSIndexTest(base.TestCase):
    @staticmethod
    def test_rcu_index_2_signal_index():
        """Test code for rcu_index_2_signal_index"""

        test_input = []
        test_output = []

        # test 1
        test_input.append(np.array([0]))
        test_output.append(np.array([0, 2, 4]))

        # test 2
        test_input.append(np.array([1]))
        test_output.append(np.array([1, 3, 5]))

        for input_rcu_index, expected_output in zip(test_input, test_output):
            actual_output = index.rcu_index_2_signal_index(input_rcu_index)
            np.testing.assert_array_equal(expected_output, actual_output)

    @staticmethod
    def test_rcu_index_and_rcu_input_index_2_signal_index():
        """Test code for rcu_index_and_rcu_input_index_2_signal_index"""

        test_input = []
        test_output = []

        # test 1
        test_input.append([np.array([0]), np.array([0])])
        test_output.append(np.array([0]))

        # test 2
        test_input.append([np.array([0]), np.array([0, 1, 2])])
        test_output.append(np.array([0, 2, 4]))

        for inputs, expected_output in zip(test_input, test_output):
            input_rcu_index = inputs[0]
            input_rcu_input_index = inputs[1]
            actual_output = index.rcu_index_and_rcu_input_index_2_signal_index(
                input_rcu_index, input_rcu_input_index
            )
            np.testing.assert_array_equal(expected_output, actual_output)

    @staticmethod
    def test_signal_input_2_rcu_index_and_rcu_input_index():
        """Test code for signal_input_2_rcu_index_and_rcu_input_index"""

        test_input = []
        test_output = []

        # test 1
        test_input.append(np.array([0]))
        test_output.append((np.array([0]), np.array([0])))

        # test 2
        test_input.append(np.array([1]))
        test_output.append((np.array([1]), np.array([0])))

        for input_signal_index, expected_output in zip(test_input, test_output):
            actual_output = index.signal_input_2_rcu_index_and_rcu_input_index(
                input_signal_index
            )
            np.testing.assert_array_equal(expected_output, actual_output)
