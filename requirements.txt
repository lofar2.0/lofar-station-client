# Order does matter
pip>=23.3
requests>=2.31.0 # Apache 2
numpy>=1.21.0 # BSD
nptyping>=2.3.0 # MIT
matplotlib>=3.5.0 # PSF
pyDeprecate>=0.3.0 # MIT
h5py >= 3.1.0 # BSD
pytz>=2022.6 # MIT
psutil>=5.9.4 # BSD
pyzmq>=24 # LGPL + BSD
