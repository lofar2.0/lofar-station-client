#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
from typing import Dict

from lofar_station_client.file_access._lazy_dict import lazy_dict
from tests import base


class TestLazyDict(base.TestCase):
    def test_dict_read(self):
        invocations = []
        data = {1: 99, 8: 55, 98: 3}
        dict_type = Dict[int, int]

        def reader(key):
            invocations.append(f"Invoked with {key}")
            return data[key]

        d1 = lazy_dict(dict_type, reader)
        d1[1] = lambda: reader(1)
        d1[8] = lambda: reader(8)
        d1[98] = lambda: reader(98)

        self.assertEqual(99, d1[1])
        self.assertEqual(55, d1[8])
        self.assertEqual(3, d1[98])
        self.assertEqual("Invoked with 1", invocations[0])
        self.assertEqual("Invoked with 8", invocations[1])
        self.assertEqual("Invoked with 98", invocations[2])

    def test_dict_write(self):
        invocations = []
        data = {}
        dict_type = Dict[int, int]

        def reader(key):
            invocations.append(f"Invoked with {key}")
            return data[key]

        def writer(key, value):
            invocations.append(f"Invoked with {key} = {value}")
            data[key] = value

        d1 = lazy_dict(dict_type, reader)
        d1.setup_write(writer)

        d1[1] = 2
        self.assertEqual("Invoked with 1 = 2", invocations[0])
        d1[2] = 3
        self.assertEqual("Invoked with 2 = 3", invocations[1])
        d1[1] = 4
        self.assertEqual("Invoked with 1 = 4", invocations[2])

        self.assertEqual(4, d1[1])
        self.assertEqual(3, d1[2])
        self.assertEqual("Invoked with 1", invocations[3])
        self.assertEqual("Invoked with 2", invocations[4])
