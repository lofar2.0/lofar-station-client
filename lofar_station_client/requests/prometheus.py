# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Contains classes and method to facilitate interacting with prometheus instances"""

# too-few-public-methods, too-many-method-arguments, too-many-local-variables
# pylint: disable=R0903, R0913, R0914

from collections import OrderedDict
from datetime import datetime
from datetime import timedelta
from http import HTTPStatus
from typing import Callable
from typing import List
from typing import Tuple
from typing import Union
import json
import urllib
import requests

from lofar_station_client.parsing.parse import validate_types


class PrometheusRequests:
    """Make requests to prometheus instances"""

    datetime_or_epoch = Union[datetime, float]

    QUERY_PATH = "/api/v1/query_range?query="

    DEFAULT_HOST = "http://prometheus:9090"
    DEFAULT_DURATION = 3600  # in seconds
    DEFAULT_STEP_SIZE = 60  # in seconds
    DEFAULT_TIMEOUT = 60.0  # in seconds

    @staticmethod
    def get_attribute_history(  # pylint: disable=too-many-positional-arguments
        attribute: str,
        device: str,
        host: str = DEFAULT_HOST,
        start: datetime_or_epoch = None,
        end: datetime_or_epoch = None,
        step: int = DEFAULT_STEP_SIZE,
    ) -> List[  # pylint: disable=too-many-positional-arguments
        List[List[Tuple[datetime, any]]]
    ]:
        """Retrieve prometheus history for a specific attribute from a specific device

        :param attribute: The device attribute to retrieve history about
        :param device: The target device
        :param host: The prometheus host, can be left blank
        :param start: Beginning of range to retrieve history between
        :param end: End of range to retrieve history between
        :param step: Time between individual values in seconds, can be left blank
        :return: multidimensional array of values and timestamp tuples for the
                 specified device and attribute where timestamps are between
                 start and end
        :raises TypeError: Raised if any arguments do not adhere to the type hints
        :raises ValueError: Raised if start time in the future or url invalid
        :raises RuntimeError: Raised upon http or prometheus request error
        """

        path = PrometheusRequests.QUERY_PATH

        # This is an end user input function so validate argument types
        _type_validators = [
            (attribute, [str], "attribute"),
            (device, [str], "device"),
            (host, [str], "host"),
            (start, [None, datetime, float], "start"),
            (end, [None, datetime, float], "end"),
            (step, [int], "step"),
        ]
        validate_types(_type_validators)

        # Determine duration
        start, end = PrometheusRequests._default_duration(start, end)

        # Verify start argument is not set in the future
        if start and isinstance(start, datetime) and start > datetime.now():
            raise ValueError("Start time can not be in the future.")

        # Convert datetime objects to strings, shadow previous variables
        start = start.strftime("%Y-%m-%dT%H:%M:%SZ")
        end = end.strftime("%Y-%m-%dT%H:%M:%SZ")

        query = urllib.parse.quote(
            f'device_attribute{{name="{attribute}", device="{device.lower()}"}}'
        )

        # Perform the actual get request
        http_result = requests.get(
            f"{host}{path}{query}&start={start}&end={end}&step={step}",
            timeout=PrometheusRequests.DEFAULT_TIMEOUT,
        )

        # Check status code for any errors, only OK 200 accepted
        if HTTPStatus(http_result.status_code) is not HTTPStatus.OK:
            prometheus_error = ""
            if http_result.text:
                prometheus_error = json.loads(http_result.text)["error"]

            raise RuntimeError(
                f"Failed to retrieve attribute history HTTP error: "
                f"{HTTPStatus(http_result.status_code).description}, "
                f"{prometheus_error}"
            )

        json_results = json.loads(http_result.text)

        # These objects can be quite large so 'free' memory as soon as possibles
        http_result = None

        # Verify prometheus response indicates success
        if json_results["status"] != "success":
            raise RuntimeError(
                f"Failed to retrieve attribute history Prometheus error: "
                f"{json_results['status']}, "
                f"{json_results['error']}"
            )

        # Update json_results to only contain results array, reduce memory
        json_results = json_results["data"]["result"]

        # In-place conversion of all string types based on their str_value
        PrometheusRequests._convert_str_value(json_results)

        # Merge split results identified by x and y indices, shallow copies
        # unsetting json_results has no effect on memory
        result_sets = PrometheusRequests._merge_result_data(json_results)

        # Expand the results into [x][y][tuple[datetime, value]] with converted values
        return PrometheusRequests._expand_convert_results_3d(result_sets)

    @staticmethod
    def _default_duration(
        start: datetime_or_epoch, end: datetime_or_epoch
    ) -> Tuple[datetime, datetime]:
        """generates the default duration if start and / or end is set

        :return: tuple containing the start and stop datetime objects
        """

        # Convert floats to datetime objects
        if isinstance(start, float):
            start = datetime.fromtimestamp(start)
        if isinstance(end, float):
            end = datetime.fromtimestamp(end)

        # If start is set but no end, adjust end to default duration from start
        if start and end is None:
            end = start + timedelta(seconds=PrometheusRequests.DEFAULT_DURATION)

        # If end is still None start must have been unset, base duration on end
        if end is None:
            end = datetime.now()

        # If start is None base duration on end
        if start is None:
            start = end - timedelta(seconds=PrometheusRequests.DEFAULT_DURATION)

        return start, end

    @staticmethod
    def _get_type_converter(type_str: str) -> Callable:
        """Convert the type string provided by prometheus to a callable converter

        Cannot be used for str_value conversion only operates on values,
        """

        if type_str == "bool":
            return lambda x: json.loads(x.lower())
        if type_str in ("float", "state"):
            return float

        return lambda x: x

    @staticmethod
    def _convert_str_value(json_results: dict):
        """In place conversion of string values

        :param: json_results Dictionary with prometheus json result array
                     pass as `json["data"]["result"]` from raw json.
        """

        for json_result in json_results:
            # Only convert type string
            if json_result["metric"]["type"] != "string":
                continue

            # Get str_value or empty as default
            str_value = json_result.get("metric").get("str_value")
            if not str_value:
                str_value = ""

            for value in json_result["values"]:
                value[1] = str_value

    @staticmethod
    def _merge_result_data(json_results: dict) -> OrderedDict:
        """Merge all result entries for matching x,y,attribute,station combinations

        :param json_results: Dictionary with prometheus json result array
                             pass as `json["data"]["result"]` from raw json.
        """

        def ord_index(metric: dict) -> Tuple[str]:
            """Generate the ordered dict index from json metric"""
            return (metric["x"], metric["y"])

        result_dict = OrderedDict()
        for json_result in json_results:
            dict_key = ord_index(json_result["metric"])

            # New coordinate
            if result_dict.get(dict_key) is None:
                result_dict[dict_key] = json_result
                continue

            # Existing coordinate, extend
            result_dict[dict_key]["values"].extend(json_result["values"])

        return result_dict

    @staticmethod
    def _expand_convert_results_3d(
        merged_results: OrderedDict,
    ) -> List[List[List[Tuple[datetime, any]]]]:
        """Expand the results into [y][x][tuple[datetime, value]] and convert values

        :param merged_results: Result from _merge_result_data
        """

        def str_to_coords(coordinate: Tuple[str]) -> (int, int):
            """Convert the string coordinate"""
            return int(coordinate[0]), int(coordinate[1])

        def convert_row(row: List[any], converter: Callable):
            return datetime.fromtimestamp(row[0]), converter(row[1])

        results = [[[]]]
        for key, values in merged_results.items():
            x_index, y_index = str_to_coords(key)

            # Store as [y][x], just like Tango

            # Ensure outermost array sufficiently sized
            while len(results) <= y_index:
                results.append([[]])

            # Ensure internal array sufficiently sized
            while len(results[y_index]) <= x_index:
                results[y_index].append([])

            # Per result determine the callable type converter
            type_converter = PrometheusRequests._get_type_converter(
                values["metric"]["type"]
            )

            result = [convert_row(row, type_converter) for row in values["values"]]
            results[y_index][x_index].extend(result)

        return results
