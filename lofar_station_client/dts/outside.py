# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""DTS outside functionality such as restarting and configuring

Functions to:
- restart the Dwingeloo Test Station (Outside)
- configure the Dwingeloo Test Station (Outside) to a defined default

Boudewijn Hut
"""

import time
import datetime
import os
import logging

import numpy as np

from lofar_station_client.dts import common
from lofar_station_client.dts import constants
from lofar_station_client.dts import plot

folders = [constants.PLOT_DIR]
for folder in folders:
    if not os.path.exists(folder):
        os.makedirs(folder)

logger = logging.getLogger()


def init_and_set_to_default_config(devices, skip_boot=False, silent=False):
    """Restart system and set to default configuration

    :param devices: struct of software devices of m&c of the station name

    :return: True if an error ocurred, False otherwise
    """

    original_log_level = logger.getEffectiveLevel()
    if silent:
        logger.setLevel(logging.WARNING)

    found_error = False
    # initialisation
    if not skip_boot:
        logger.info("Start initialisation")
        found_error = found_error | initialise(devices)
    else:
        logger.info("SKIPPING INITIALISATION (skip_init was set to True)")
    logger.info("Check initialisation")
    found_error = found_error | check_initialise(devices)
    # configuring
    logger.info("Start setting to default configuration")
    found_error = found_error | set_to_default_configuration(devices)
    logger.info("Check setting to default configuration")
    found_error = found_error | check_set_to_default_configuration(
        devices, readonly=False
    )  # readonly=False, only at first time after init
    # plot statistics data
    logger.info("Plot statistics data")
    plot.plot_statistics(devices)

    if silent and original_log_level != logging.NOTSET:
        logger.setLevel(original_log_level)

    return found_error


def initialise(devices, timeout=90):
    """Initialise the system

    :param timeout: Timeout to flag an error in seconds
    :return: true if error, false otherwise
    """

    devices = common.devices_list_2_dict(devices)
    boot = devices["boot"]
    recv = devices["recv"]
    sst = devices["sst"]
    unb2 = devices["unb2"]

    # initialise boot device
    boot.put_property({"DeviceProxy_Time_Out": 60})
    boot.off()
    boot.initialise()
    boot.on()

    # increase timeouts for reboot
    logger.debug("Time out settings prior to increase for reboot:")
    logger.debug(recv.get_timeout_millis())
    logger.debug(unb2.get_timeout_millis())
    logger.debug(sst.get_timeout_millis())

    recv.set_timeout_millis(30000)
    unb2.set_timeout_millis(60000)
    sst.set_timeout_millis(20000)

    unb2.put_property({"UNB2_On_Off_timeout": 20})
    recv.put_property({"RCU_On_Off_timeout": 60})

    # reboot system
    boot.reboot()
    logger.info("Rebooting now")
    time_start = time.time()
    time_dt = time.time() - time_start

    # indicate progress
    while boot.booting_R and (time_dt < timeout):
        if time_dt % 5 < 1:  # print every 5 seconds
            logger.info("Initialisation at %f%%: %s", boot.progress_R, boot.status_R)
        time.sleep(1)
        time_dt = time.time() - time_start
    logger.info("Initialisation took %d seconds", time_dt)
    # check for errors
    found_error = False
    if boot.booting_R:
        found_error = True
        logger.warning(
            "Warning! Initialisation still ongoing after timeout (%d sec)", timeout
        )
    if boot.uninitialised_devices_R:
        found_error = True
        logger.error("Warning! Did not initialise %s.", boot.uninitialised_devices_R)
    return found_error


def check_initialise(devices):
    """Check the initialisation of the system by an independent monitoring point.

    Note that this method should be used after calling initialise()

    All checks that are directly related to the boot device are already
    in the method initialise()
    """

    found_error = False
    found_error = found_error | check_firmware_loaded(devices)
    found_error = found_error | check_clock_locked(devices)
    return found_error


def check_firmware_loaded(devices):
    """Verify that the firmware has loaded in the fpgas

    :return: True if not loaded, False otherwise
    """
    devices = common.devices_list_2_dict(devices)
    sdp = devices["sdp"]
    res = sdp.FPGA_boot_image_R
    logger.debug("")
    logger.debug("Checking sdp.FPGA_boot_image_R:")
    logger.debug("Reply: %s", res)
    if not np.array_equal(res[0 : constants.N_FPGA], np.ones(constants.N_FPGA)):
        logger.debug("Firmware is not loaded!")
        return True
    logger.debug("Ok - Firmware loaded")
    return False


def check_clock_locked(devices):
    """Verify that the APSCT clock is locked

    :return: True if not loaded, False otherwise
    """

    devices = common.devices_list_2_dict(devices)
    apsct = devices["apsct"]
    res = apsct.APSCT_PLL_200MHz_error_R

    logger.debug("")
    logger.debug("Checking apsct.APSCT_PLL_200MHz_error_R:")
    logger.debug("Reply: %s", res)
    if res:
        logger.debug("APSCT clock not locked!")
        return True
    logger.debug("Ok - APSCT clock locked")
    return False


def set_to_default_configuration(devices):
    """Set the station to its default configuration

    :return: True if error found, False otherwise
    """

    found_error = False
    logger.info("Start configuring Low Receivers")
    found_error = found_error | set_rcul_to_default_config(devices)
    logger.info("Start configuring High Receivers")
    found_error = found_error | set_rcuh_to_default_config(devices)
    logger.info("Start configuring Station Digital Processor")
    found_error = found_error | set_sdp_to_default_config(devices)
    logger.info("Start configuring Subband Statistics")
    found_error = found_error | set_sdp_sst_to_default_config(devices)
    logger.info("Start configuring Crosslet Statistics")
    found_error = found_error | set_sdp_xst_to_default_config(devices)
    logger.info("Start configuring Beamlet Statistics")
    found_error = found_error | set_sdp_bst_to_default_config(devices)
    return found_error


def set_rcul_to_default_config(devices):
    """Set RCU Low to its default configuration"""

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]

    # Set RCUs Lows in default modes

    recv.set_defaults()

    # Set attenuator, antenna power etc.
    recv.RCU_band_select_RW = set_rcu_ant_masked(
        recv, constants.LBA_MASK, recv.RCU_band_select_R, [[1] * 3] * 32
    )  # 1 = 30-80 MHz
    recv.RCU_PWR_ANT_on_RW = set_rcu_ant_masked(
        recv, constants.LBA_MASK, recv.RCU_PWR_ANT_on_R, [[True] * 3] * 32
    )  # Off
    recv.RCU_attenuator_dB_RW = set_rcu_ant_masked(
        recv, constants.LBA_MASK, recv.RCU_attenuator_dB_R, [[0] * 3] * 32
    )  # 0dB attenuator
    wait_rcu_receiver_busy(recv)

    #
    found_error = False
    return found_error


def set_rcuh_to_default_config(devices):
    """Set RCU High to its default configuration"""

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]

    # Setup HBA RCUs and HBA Tiles

    # Set RCU in correct mode
    rcu_modes = recv.RCU_band_select_R
    rcu_modes[8] = [2, 2, 2]  # 2 = 110-180 MHz, 1 = 170-230 MHz, 4 = 210-270 MHz ??
    rcu_modes[9] = [2, 2, 2]
    recv.RCU_band_select_RW = set_rcu_ant_masked(
        recv, constants.HBA_MASK, recv.RCU_band_select_R, rcu_modes
    )
    wait_rcu_receiver_busy(recv)

    # Switch on the Antenna
    recv.RCU_PWR_ANT_on_RW = set_rcu_ant_masked(
        recv, constants.HBA_PWR_MASK, recv.RCU_PWR_ANT_on_R, [[False] * 3] * 32
    )  # Switch off
    wait_rcu_receiver_busy(recv)
    recv.HBAT_PWR_LNA_on_RW = set_rcu_hba_mask(
        recv, constants.HBA_CTL_MASK, recv.HBAT_PWR_LNA_on_R, [[True] * 32] * 96
    )  # LNA default on
    recv.HBAT_BF_delay_steps_RW = set_rcu_hba_mask(
        recv, constants.HBA_CTL_MASK, recv.HBAT_BF_delay_steps_R, [[0] * 32] * 96
    )  # Default
    recv.HBAT_PWR_on_RW = set_rcu_hba_mask(
        recv, constants.HBA_CTL_MASK, recv.HBAT_PWR_on_R, [[True] * 32] * 96
    )  # Default
    wait_rcu_receiver_busy(recv)
    recv.RCU_PWR_ANT_on_RW = set_rcu_ant_masked(
        recv, constants.HBA_PWR_MASK, recv.RCU_PWR_ANT_on_R, [[True] * 3] * 32
    )  # Switch on
    wait_rcu_receiver_busy(recv)
    # default in the tile: after power-on, zero delay
    # delays should be set by the tile beam device
    # recv.HBAT_BF_delay_steps_RW=[[1]*32]*32
    # wait_receiver_busy(recv)
    # by default: equal delay settings for all elements --> Pointing to zenith

    # TODO(Boudewijn): read values and only update the ones that need to be changed.
    # Could be a function that manages the masks.
    logging.info(recv.RCU_PWR_ANT_on_RW)
    logging.info("")
    logging.info("done")

    found_error = False
    return found_error


####
# RCU specific functions and variables for DTS-Outside
####

# General RCU functions


def wait_rcu_receiver_busy(recv):
    """Wait for the Receiver Translators busy monitoring point to return False"""

    while recv.RECVTR_translator_busy_R:
        time.sleep(0.1)


def set_rcu_ant_masked(recv, mask, old_value, new_value):
    """Set the antenna mask for the Receiver Translator"""

    recv.ANT_mask_RW = mask

    for idx, mask_elem in enumerate(mask):
        # TODO(Boudewijn): Check that range(3) still is correct dimensionality
        for rcu_input_index in range(3):
            if mask_elem[rcu_input_index]:
                old_value[idx][rcu_input_index] = new_value[idx][rcu_input_index]
    return old_value


def set_rcu_hba_mask(recv, mask, old_value, new_value):
    """Set the hba mask for the Receiver Translator

    :param mask: Takes flattened one dimensional array of values
    :param old_value: Takes flattened one dimensional array of values
    :param new_value: Takes flattened one dimensional array of values
    """

    recv.ANT_mask_RW = mask
    for idx, mask_elem in enumerate(mask):
        # TODO(Boudewijn): Check that range(3) still is correct dimensionality
        for i in range(3):
            if mask_elem[i]:
                old_value[idx * 3 + i] = new_value[idx * 3 + i]
            else:
                old_value[idx * 3 + i] = old_value[idx * 3 + i]
    return old_value


def set_sdp_to_default_config(devices):
    """Set RCU High to its default configuration

    TODO(Corne): Update this docstring to reflect actual method functionality
    """

    devices = common.devices_list_2_dict(devices)
    sdp = devices["sdp"]
    #
    # Set SDP in default modes
    #

    logging.info("Start configuring SDP to default")
    sdp.set_defaults()
    # should be part of sdp.set_defaults:
    next_ring = [False] * 16
    next_ring[3] = True
    sdp.FPGA_ring_use_cable_to_previous_rn_RW = [True] + [False] * 15
    sdp.FPGA_ring_use_cable_to_next_rn_RW = next_ring
    sdp.FPGA_ring_nof_nodes_RW = [constants.N_FPGA] * 16
    sdp.FPGA_ring_node_offset_RW = [0] * 16

    #
    found_error = False
    return found_error


def set_sdp_sst_to_default_config(devices):
    """Set SSTs to default"""

    devices = common.devices_list_2_dict(devices)
    sst = devices["sst"]

    sst.set_defaults()
    # prepare for subband stati
    sst.FPGA_sst_offload_weighted_subbands_RW = [
        True
    ] * 16  # should be in set_defaults()

    #
    found_error = False
    return found_error


def set_sdp_xst_to_default_config(devices):
    """Set XSTs to default"""

    devices = common.devices_list_2_dict(devices)
    xst = devices["xst"]

    # prepare for correlations
    int_time = 1  # used for 'medium' measurement using statistics writer
    subband_step_size = 7  # 0 is no stepping
    n_xst_subbands = 7
    subband_select = [subband_step_size, 0, 1, 2, 3, 4, 5, 6]

    xst.set_defaults()
    # should be part of set_defaults()
    xst.FPGA_xst_processing_enable_RW = [False] * 16
    # this (line above) should be first, then configure and enable
    xst.fpga_xst_ring_nof_transport_hops_RW = [3] * 16  # [((n_fgpa/2)+1)]*16

    crosslets = [0] * 16
    for fpga_nr in range(constants.N_FPGA):
        crosslets[fpga_nr] = n_xst_subbands
    xst.FPGA_xst_offload_nof_crosslets_RW = crosslets

    xst.FPGA_xst_subband_select_RW = [subband_select] * 16
    xst.FPGA_xst_integration_interval_RW = [int_time] * 16

    xst.FPGA_xst_processing_enable_RW = [True] * 16
    #
    found_error = False
    return found_error


def set_sdp_bst_to_default_config(devices):
    """Set XSTs to default"""

    devices = common.devices_list_2_dict(devices)
    beamlet = devices["beamlet"]

    # prepare for beamformer
    beamlet.set_defaults()

    beamlet.FPGA_bf_ring_nof_transport_hops_RW = [[1]] * 3 + [[0]] * 13
    # line above should be part of set_defaults(), specifically for DTS-Outside

    beamletoutput_mask = [False] * 16
    beamletoutput_mask[3] = True
    # beamlet.FPGA_beamlet_output_enable_RW=beamletoutput_mask
    beamlet.FPGA_beamlet_output_hdr_eth_destination_mac_RW = ["40:a6:b7:2d:4f:68"] * 16
    beamlet.FPGA_beamlet_output_hdr_ip_destination_address_RW = ["192.168.0.249"] * 16

    # define weights
    # dim: FPGA, input, subband
    weights_xx = beamlet.FPGA_bf_weights_xx_R.reshape([16, 6, 488])
    weights_yy = beamlet.FPGA_bf_weights_yy_R.reshape([16, 6, 488])
    # make a beam for the HBA
    weights_xx[:][:] = 0
    weights_yy[:][:] = 0
    for signal_index in [0, 1]:  # range(8*3,9*3+3):
        unb2i = signal_index // 12
        signal_index_2 = signal_index // 2
        signal_index_2 = signal_index_2 % 6
        if signal_index % 2 == 0:
            weights_xx[unb2i, signal_index_2] = 16384
        else:
            weights_yy[unb2i, signal_index_2] = 16384

    beamlet.FPGA_bf_weights_xx_RW = weights_xx.reshape([16, 6 * 488])
    beamlet.FPGA_bf_weights_yy_RW = weights_yy.reshape([16, 6 * 488])

    blt = beamlet.FPGA_beamlet_subband_select_R.reshape([16, 12, 488])
    blt[:] = np.array(range(10, 10 + 488))[np.newaxis, np.newaxis, :]
    beamlet.FPGA_beamlet_subband_select_RW = blt.reshape([16, 12 * 488])

    #
    found_error = False
    return found_error


def enable_outputs(
    devices, enable_sst=True, enable_xst=True, enable_bst=True, enable_beam_output=True
):
    """Enable the station outputs after everything is set."""

    devices = common.devices_list_2_dict(devices)
    if enable_beam_output:
        sdp = devices["sdp"]
        sdp.FPGA_processing_enable_RW = [True] * 16
    if enable_sst:
        sst = devices["sst"]
        sst.FPGA_sst_offload_enable_RW = [True] * 16
    if enable_xst:
        xst = devices["xst"]
        xst.FPGA_xst_offload_enable_R = [True] * 16
    if enable_bst:
        beamlet = devices["beamlet"]
        beamlet.FPGA_beamlet_output_enable_RW = [i == 3 for i in range(16)]

    #
    found_error = False
    return found_error


def check_set_to_default_configuration(devices, readonly=True):
    """Check the default configuration of the system to be set correctly."""

    found_error = False
    found_error = found_error | check_default_configuration_low_receivers(devices)
    found_error = found_error | check_default_configuration_hbat(
        devices, readonly=readonly
    )
    found_error = found_error | check_rcu_fpga_interface(devices, si_to_check=range(18))
    found_error = found_error | check_set_sdp_xst_to_default_config(
        devices, lane_mask=range(0, 6)
    )

    plot.print_fpga_input_statistics(devices)

    return found_error


def check_default_configuration_low_receivers(devices, mask=None):
    """Check that the LNAs are on"""

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    found_error = False

    if mask is None:
        mask = constants.LBA_MASK

    res = recv.RCU_PWR_ANT_on_R
    if len(mask) > 0:
        res = res[mask]
    logging.debug("")
    logging.debug("Checking recv.RCU_PWR_ANT_on_R:")
    logging.debug("Reply%s: %s}", " (masked)" if len(mask) > 0 else "", res)
    if not res.all():
        logging.info("One or more LBAs are powered off!")
        found_error = True
    else:
        logging.debug("Ok - Low Receivers operational")
    return found_error


def check_default_configuration_hbat(devices, readonly=True):
    """Check that the HBA Tiles are on"""

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    hbat_si = [rcu * 3 + r_input for rcu in constants.RCU2HPWR for r_input in range(3)]
    if not readonly:
        recv.RECVTR_monitor_rate_RW = 1  # for this checking
        time.sleep(3)

    found_error = False
    found_error = found_error | check_rcu_vin(devices, si_mask=hbat_si)
    found_error = found_error | check_hbat_vout(devices, si_mask=hbat_si)
    found_error = found_error | check_hbat_iout(devices, si_mask=hbat_si)

    if not readonly:
        recv.RECVTR_monitor_rate_RW = 10

    # TODO(Boudewijn): - what is checked for here?
    logging.info(recv.HBAT_PWR_LNA_on_R[8:10])
    logging.info(recv.ANT_mask_RW[8:10])

    return found_error


def check_rcu_vin(devices, si_mask=None, valid_range=None):
    """Verify HBA Tile is powered by comparing V_in against signal inputs thresholds.

    If no si_mask is given, all returned values are compared.
    """

    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    if si_mask is None:
        si_mask = []
    if valid_range is None:
        valid_range = [30, 50]

    res = recv.RCU_PWR_ANT_VIN_R
    if len(si_mask) > 0:
        res = [res[si // 3][si % 3] for si in si_mask]
    logger.debug("")
    logger.debug("Checking recv.RCU_PWR_ANT_VIN_R:")
    logger.debug("Reply%s: %s", " (masked)" if len(si_mask) > 0 else "", res)
    if not (
        np.all(valid_range[0] < np.array(res))
        and np.all(np.array(res) < valid_range[1])
    ):
        logging.warning("No power at input RCU2!")
        return True

    logging.debug("Ok - Power at input RCU2")
    return False


def check_hbat_vout(devices, si_mask=None, valid_range=None):
    """Verify HBA Tile is powered by comparing V_out against signal input thresholds

    If no si_mask is given, all returned values are compared.
    """

    # Vout should be 48 +- 1 V
    devices = common.devices_list_2_dict(devices)

    recv = devices["recv"]
    if si_mask is None:
        si_mask = []
    if valid_range is None:
        valid_range = [30, 50]

    res = recv.RCU_PWR_ANT_VOUT_R
    if len(si_mask) > 0:
        res = [res[si // 3][si % 3] for si in si_mask]

    logging.debug("")
    logging.debug("Checking recv.RCU_PWR_ANT_VOUT_R:")
    logging.debug("Reply%s: %s", " (masked)" if len(si_mask) > 0 else "", res)

    if not (
        np.all(valid_range[0] < np.array(res))
        and np.all(np.array(res) < valid_range[1])
    ):
        logging.warning("Not all HBA Tiles powered on!")
        return True

    logging.debug("Ok - HBA Tiles powered on")
    return False


def check_hbat_iout(devices, si_mask=None, valid_range=None):
    """Verify HBA Tile is powered by comparing I_out against signal input thresholds

    If no si_mask is given, all returned values are compared.
    """

    # Iout = 0.7 +- 0.1 Amp
    devices = common.devices_list_2_dict(devices)
    recv = devices["recv"]
    if si_mask is None:
        si_mask = []
    if valid_range is None:
        valid_range = [0.6, 0.8]

    res = recv.RCU_PWR_ANT_IOUT_R
    if len(si_mask) > 0:
        res = [res[si // 3][si % 3] for si in si_mask]
    logging.debug("")
    logging.debug("Checking recv.RCU_PWR_ANT_IOUT_R:")
    logging.debug("Reply%s: %s", " (masked)" if len(si_mask) > 0 else "", res)
    if not (
        np.all(valid_range[0] < np.array(res))
        and np.all(np.array(res) < valid_range[1])
    ):
        logging.warning("Not all HBA Tiles powered on (draw current)!")
        return True

    logging.debug("Ok - HBA Tiles powered on (draw current)")
    return False


def check_rcu_fpga_interface(devices, si_to_check=range(18)):
    """Function to check the RCU - FPGA interface

    if error occurs, try to reboot by resetting Uniboard (switch)
    if only one works than the APSCT board is not making contact with the backplane.
    (Can take up to 3 times to get it right..)
    23/6/2022T15:42 errorcode 0x4h: xxxxx on all of them.
    Error descriptions can be found here:
    https://support.astron.nl/jira/browse/L2SDP-774
    """
    devices = common.devices_list_2_dict(devices)
    sdp = devices["sdp"]
    found_error = False
    # TODO(Boudewijn): the number of calls can be reduced
    for signal_index in si_to_check:
        lock = sdp.FPGA_jesd204b_csr_dev_syncn_R[signal_index // 12][signal_index % 12]
        if lock != 1:
            found_error = True
            print(f"Transceiver SI: {signal_index} is not locked {lock}")
        err0 = sdp.FPGA_jesd204b_rx_err0_R[signal_index // 12][signal_index % 12]
        if err0 != 0:
            found_error = True
            print(f"SI {signal_index} has JESD err0: 0x{err0:x}h")
        err1 = sdp.FPGA_jesd204b_rx_err1_R[signal_index // 12][signal_index % 12]
        if (err1 & 0x02FF) != 0:
            found_error = True
            print(f"SI {signal_index} has JESD err1: 0x{err1:x}h")
    return found_error


def check_set_sdp_xst_to_default_config(
    devices, lane_mask=None, time_1=None, time_dt=2.5
):
    """Check sdp/xst configuration by checking the returned timestamps for:

        1. all being the same
        2. not epoch zero (at 1 Jan 1970 00:00:00)
        3. within 5 seconds from the local clock timestamp (now +/- 2.5 sec)

    :param time_1: number of seconds since the epoch to compare against
                   Default: None --> now()
    :param time_dt: delta in seconds around time_1, in which the xst timestamps
                    should fall.
    """

    devices = common.devices_list_2_dict(devices)
    xst = devices["xst"]
    if lane_mask is None:
        lane_mask = []
    if time_1 is None:
        time_1 = datetime.datetime.now().timestamp()
    res = xst.xst_timestamp_R
    if len(lane_mask) > 0:  # mask lanes
        res = [res[lane] for lane in lane_mask]
    logging.debug("")
    logging.debug("Checking xst.xst_timestamp_R:")
    logging.debug("Reply%s: %s", " (masked)" if len(lane_mask) > 0 else "", res)
    logging.debug("Converted:")
    logging.debug(
        "\n".join([f"{time.asctime(time.gmtime(xst_time))}" for xst_time in res])
    )
    if not (res == res[0]).all():
        logging.warning("Not all xst timestamps are equal!")
        return True
    if res[0] == 0:
        logging.warning("One or more xst timestamps are zero!")
        return True
    if abs(res[0] - time_1) > time_dt:
        logging.warning(
            "One or more xst timestamps are outside the specified time window!"
        )
        return True
    logging.debug("Ok - xst configured correctly")
    return False
