# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import copy
import math
from datetime import datetime
from datetime import timedelta
import json

from unittest import mock

from lofar_station_client.requests import prometheus
from lofar_station_client.requests.prometheus import PrometheusRequests

from tests import base


class PrometheusTest(base.TestCase):
    # Multiple results for identical (x, y, attribute, station) combinations
    DUMMY_RESULT_DATA_DUPLICATE = """
    {
        "status":"success",
        "data":{
            "resultType":"matrix",
            "result":[{
                "metric":{
                    "__name__":"device_attribute",
                    "device":"stat/recv/1",
                    "host":"localhost","idx":"000",
                    "instance":"tango-prometheus-exporter:8000",
                    "job":"tango",
                    "name":"RCU_TEMP_R",
                    "station":"DevStation",
                    "type":"float",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1654560000,"24.192592000000047"],
                    [1654560060,"24.192592000000047"],
                    [1654560120,"24.192592000000047"],
                    [1654646040,"NaN"]
                ]},{
                "metric":{
                    "__name__":"device_attribute",
                    "device":"stat/recv/1",
                    "host":"localhost",
                    "idx":"001",
                    "instance":"tango-prometheus-exporter:8000",
                    "job":"tango",
                    "name":"RCU_TEMP_R",
                    "station":"DevStation",
                    "type":"float",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1654560050,"24.927376000000038"],
                    [1654560070,"24.927376000000038"],
                    [1654560140,"24.927376000000038"],
                    [1654560200,"24.927376000000038"]
                ]}
            ]
        }
    }
    """

    # Most complicated example requiring reassignment of str_value
    DUMMY_RESULT_DATA_DUPLICATE_STRINGS = """
    {
        "status":"success",
        "data":{
            "resultType":"matrix",
            "result":[{
                "metric":{
                    "__name__":"device_attribute",
                    "device":"stat/sst/1",
                    "host":"localhost",
                    "idx":"000",
                    "instance":"tango-prometheus-exporter:8000",
                    "job":"tango",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "station":"DTSOutside",
                    "str_value":"10.99.0.254",
                    "type":"string",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1655790600,"11"],
                    [1655791200,"11"],
                    [1655791800,"11"],
                    [1655792400,"11"]
                ]},{
                "metric":{
                    "__name__":"device_attribute",
                    "device":"stat/sst/1",
                    "host":"localhost",
                    "idx":"000",
                    "instance":"tango-prometheus-exporter:8000",
                    "job":"tango",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "station":"DTSOutside",
                    "str_value":"63.227.66.0",
                    "type":"string",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1655737200,"11"],
                    [1655737800,"11"]
                ]},{
                "metric":{
                    "__name__":"device_attribute",
                    "device":"stat/sst/1",
                    "host":"localhost",
                    "idx":"000",
                    "instance":"tango-prometheus-exporter:8000",
                    "job":"tango",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "station":"DTSOutside",
                    "type":"string",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1655725200,"0"],
                    [1655725800,"0"],
                    [1655726400,"0"],
                    [1655727000,"0"]
                ]}
            ]
        }
    }
    """

    DUMMY_RESULT_DATA_ORDERING = """
    {
        "status":"success",
        "data":{
            "resultType":"matrix",
            "result":[{
                "metric":{
                    "device":"stat/sst/1",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "str_value":"10.99.0.254",
                    "type":"string",
                    "x":"00",
                    "y":"00"
                },"values":[
                    [1655790600,"0"]
                ]},{
                "metric":{
                    "device":"stat/sst/1",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "str_value":"63.227.66.0",
                    "type":"string",
                    "x":"01",
                    "y":"00"
                },"values":[
                    [1655737800,"0"]
                ]},{
                "metric":{
                    "device":"stat/sst/1",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "type":"string",
                    "x":"00",
                    "y":"01"
                },"values":[
                    [1655725200,"0"]
                ]},{
                "metric":{
                    "device":"stat/sst/1",
                    "name":"FPGA_sst_offload_hdr_ip_destination_address_R",
                    "type":"string",
                    "x":"01",
                    "y":"01"
                },"values":[
                    [1655725200,"0"]
                ]}
            ]
        }
    }
    """

    DUMMY_RESULT_INVALID_REQUEST = """
    {
        "status": "error",
        "error": "the request was invalid"
    }
    """

    def setUp(self):
        # Patch request module inside prometheus module
        requests_patcher = mock.patch.object(prometheus, "requests")
        self.m_requests = requests_patcher.start()
        self.addCleanup(requests_patcher.stop)

    def test_default_duration(self):
        """Test that the start and stop times total to the correct duration"""

        start, end = PrometheusRequests._default_duration(None, None)
        self.assertEqual(
            PrometheusRequests.DEFAULT_DURATION, (end - start).total_seconds()
        )

    def test_default_duration_start(self):
        """Test duration using only start time in datetime format"""

        start, end = PrometheusRequests._default_duration(datetime.now(), None)
        self.assertEqual(
            PrometheusRequests.DEFAULT_DURATION, (end - start).total_seconds()
        )

    def test_default_duration_end(self):
        """Test duration using only end time in datetime format"""

        hour = datetime.now() + timedelta(seconds=PrometheusRequests.DEFAULT_DURATION)

        start, end = PrometheusRequests._default_duration(None, hour)

        # Allow for some variance in true value
        self.assertAlmostEqual(
            PrometheusRequests.DEFAULT_DURATION, (end - start).total_seconds(), delta=2
        )

    def test_default_duration_float(self):
        """Test automatic conversion of floats to datetime in _default_duration"""
        now = datetime.now()
        start = now.timestamp()
        end = (now + timedelta(seconds=PrometheusRequests.DEFAULT_DURATION)).timestamp()

        start, end = PrometheusRequests._default_duration(start, end)
        self.assertEqual(
            PrometheusRequests.DEFAULT_DURATION, (end - start).total_seconds()
        )

    def test_default_duration_mix(self):
        """Test having one argument as float and the other as datetime"""
        now = datetime.now()
        start = now
        end = (now + timedelta(seconds=PrometheusRequests.DEFAULT_DURATION)).timestamp()

        start, end = PrometheusRequests._default_duration(start, end)
        self.assertEqual(
            PrometheusRequests.DEFAULT_DURATION, (end - start).total_seconds()
        )

    def test_convert_str_value(self):
        """Test in-place conversion of string types"""

        reference_data = json.loads(PrometheusTest.DUMMY_RESULT_DATA_DUPLICATE_STRINGS)

        PrometheusRequests._convert_str_value(reference_data["data"]["result"])

        for result in reference_data["data"]["result"]:
            str_value = result.get("metric").get("str_value")
            if not str_value:
                str_value = ""

            # Each value in the tuple should be replaced with str_value
            for value in result["values"]:
                self.assertEqual(str_value, value[1])

    def test_convert_str_value_only_string(self):
        """Test that in-place conversion skips other types than string"""

        # This data does not contain string types
        reference_data = json.loads(PrometheusTest.DUMMY_RESULT_DATA_DUPLICATE)

        # Deepcopy the data to evaluate changes between them
        converted_data = copy.deepcopy(reference_data)

        # Perform the conversion, should not have any effect
        PrometheusRequests._convert_str_value(converted_data["data"]["result"])

        for result_index, result in enumerate(converted_data["data"]["result"]):
            for value_index, value in enumerate(result["values"]):
                self.assertEqual(
                    reference_data["data"]["result"][result_index]["values"][
                        value_index
                    ],
                    value,
                )

    def test_merge_result_data(self):
        """Test generation of OrderedDict for split result arrays"""

        reference_data = json.loads(PrometheusTest.DUMMY_RESULT_DATA_DUPLICATE)

        result_dict = PrometheusRequests._merge_result_data(
            reference_data["data"]["result"]
        )

        for result in reference_data["data"]["result"]:
            metric = result["metric"]
            index = (metric["x"], metric["y"])

            result_dict_timestamps = [
                result[0] for result in result_dict[index]["values"]
            ]
            result_dict_values = [result[1] for result in result_dict[index]["values"]]

            for value in result["values"]:
                self.assertIn(value[0], result_dict_timestamps)
                self.assertIn(value[1], result_dict_values)

    def assert_merge_result_order(self, result_dict):
        prev_x = 0
        prev_y = 0

        def str_to_coords(coordinate: str) -> (int, int):
            """Convert the string coordinate"""
            return int(coordinate[0]), int(coordinate[1])

        for key, result in result_dict.items():
            x, y = str_to_coords(key)
            self.assertTrue(x >= 0)
            self.assertTrue(y >= 0)

            if prev_y == y:
                self.assertTrue(
                    prev_x <= x, f"Previous x {prev_x} not below equal current {x}"
                )
                prev_y = y
            else:
                self.assertTrue(
                    prev_y <= y, f"Previous y {prev_y} not below equal current {y}"
                )
                prev_y = y
                prev_x = 0

    def test_merge_result_data_ordering(self):
        """Test that results from merge_result_data are ordered by x then y

        Order example:
        [0][0]
        [1][0]
        [0][1]
        [1][1]
        """

        reference_data = json.loads(PrometheusTest.DUMMY_RESULT_DATA_ORDERING)

        result_dict = PrometheusRequests._merge_result_data(
            reference_data["data"]["result"]
        )

        self.assert_merge_result_order(result_dict)

    def test_get_type_converter_bare(self):
        """Test type converter for type string 'string'"""

        converter = PrometheusRequests._get_type_converter("string")
        self.assertEqual("Hello World", converter("Hello World"))

    def test_get_type_converter_bool(self):
        """Test type converter for type string 'bool'"""

        converter = PrometheusRequests._get_type_converter("bool")
        self.assertEqual(True, converter("True"))

    def test_get_type_converter_float(self):
        """Test type converter for type string 'float'"""

        converter = PrometheusRequests._get_type_converter("float")

        self.assertAlmostEqual(24.6, converter("24.6"), delta=0.001)
        self.assertTrue(math.isnan(converter("NaN")))

    def test_attribute_history_parameter_types(self):
        """Test a few the argument type validations"""

        # Test single allowed type error validation
        with self.assertRaises(TypeError):
            PrometheusRequests.get_attribute_history(125, 123)

        # Test multi allowed type error validation, use None as special exception
        with self.assertRaises(TypeError):
            PrometheusRequests.get_attribute_history("valid", "valid", None)

    def test_attribute_history_valid(self):
        """Test that history is returned in expected row column format"""

        dummy_json = json.loads(PrometheusTest.DUMMY_RESULT_DATA_ORDERING)["data"][
            "result"
        ]
        dummy_metric = dummy_json[0]["metric"]

        test_response = mock.Mock(
            status_code=200, text=PrometheusTest.DUMMY_RESULT_DATA_ORDERING
        )
        self.m_requests.get.return_value = test_response

        test_result = PrometheusRequests.get_attribute_history(
            dummy_metric["name"], dummy_metric["device"]
        )

        value_count = 0
        for row in test_result:
            for column in row:
                value_count += len(column)

        self.assertEqual(len(dummy_json), value_count)

    def test_attribute_history_future(self):
        """Test that trying to retrieve history in the future raises exception"""

        with self.assertRaises(ValueError):
            PrometheusRequests.get_attribute_history(
                "dummy", "dummy", start=datetime.now() + timedelta(seconds=60)
            )

    def test_attribute_history_http_error(self):
        test_response = mock.Mock(status_code=404, text="")
        self.m_requests.get.return_value = test_response

        with self.assertRaises(RuntimeError):
            PrometheusRequests.get_attribute_history("dummy", "dummy")

    def test_attribute_history_prometheus_error(self):
        test_response = mock.Mock(
            status_code=200, text=PrometheusTest.DUMMY_RESULT_INVALID_REQUEST
        )
        self.m_requests.get.return_value = test_response

        with self.assertRaises(RuntimeError):
            PrometheusRequests.get_attribute_history("dummy", "dummy")
