#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from numpy import array

from lofar_station_client.file_access._monitoring import MonitoredWrapper
from tests import base


class TestMonitoredWrapper(base.TestCase):
    def test_list(self):
        invocations = []

        def event(a):
            invocations.append(f"Invoked with {a}")

        l1 = MonitoredWrapper(event, [])
        l1.append(1)
        self.assertEqual("Invoked with [1]", invocations[0])
        l1.append(2)
        self.assertEqual("Invoked with [1, 2]", invocations[1])
        l1.pop()
        self.assertEqual("Invoked with [1]", invocations[2])

        l2 = MonitoredWrapper(event, [1, 2, 3, 4])
        l2.append(1)
        self.assertEqual("Invoked with [1, 2, 3, 4, 1]", invocations[3])
        l2.append(2)
        self.assertEqual("Invoked with [1, 2, 3, 4, 1, 2]", invocations[4])
        l2.pop()
        self.assertEqual("Invoked with [1, 2, 3, 4, 1]", invocations[5])

        l2[0] = 99
        self.assertEqual(99, l2[0])
        self.assertEqual("Invoked with [99, 2, 3, 4, 1]", invocations[6])

        na = MonitoredWrapper(event, array([2, 3, 4]))
        self.assertEqual((3,), na.shape)
