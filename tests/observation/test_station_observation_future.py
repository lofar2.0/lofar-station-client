#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from json import dumps
from unittest import mock

from tango import DevFailed

from lofar_station_client.observation import station_observation_future
from lofar_station_client.observation.constants import (
    OBSERVATION_CONTROL_DEVICE_NAME,
    OBSERVATION_FIELD_DEVICE_NAME,
)

from tests import base
from tests.observation.specs import (
    SPEC_SINGLE_HOST_SINGLE_FIELD_DICT,
    SPEC_TWO_HOST_TWO_FIELD_DICT,
)


class TestStationObservation(base.TestCase):
    def setUp(self):
        proxy_patcher = mock.patch.object(
            station_observation_future,
            "DeviceProxy",
            autospec=True,
        )
        self.m_device_proxy = proxy_patcher.start()
        self.addCleanup(proxy_patcher.stop)

    def create_test_base(self, specification: dict, host: str):
        t_obs_future = station_observation_future.StationObservationFuture(
            specification, host
        )

        num_fields = len(specification["antenna_fields"])

        # Did create correct control device
        self.assertEqual(
            ((f"tango://{t_obs_future.host}/" f"{OBSERVATION_CONTROL_DEVICE_NAME}",),),
            self.m_device_proxy.call_args_list[0],
        )

        self.assertEqual(num_fields, len(t_obs_future._antenna_fields))
        # self.assertEqual(num_fields, len(t_obs_future._observation_field_proxies))

    def test_create_single_field(self):
        self.create_test_base(SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], "henk")

    def test_create_two_field(self):
        self.create_test_base(SPEC_TWO_HOST_TWO_FIELD_DICT["stations"][0], "henk")

    def proxies_test_base(self, specification: dict, host: str):
        t_obs_future = station_observation_future.StationObservationFuture(
            specification, host
        )

        num_fields = len(specification["antenna_fields"])
        proxies = t_obs_future.observation_field_proxies

        for index, antenna_field in enumerate(specification["antenna_fields"]):
            observation_id = antenna_field["observation_id"]
            antenna_field = antenna_field["antenna_field"]

            # Did create correct observation field device
            self.assertEqual(
                (
                    (
                        f"tango://{t_obs_future.host}/{OBSERVATION_FIELD_DEVICE_NAME}/"
                        f"{observation_id}-{antenna_field}",
                    ),
                ),
                self.m_device_proxy.call_args_list[index + 1],
            )

        self.assertEqual(num_fields, len(proxies))

    def test_proxies_single_field(self):
        self.proxies_test_base(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], "host"
        )

    def test_proxies_two_field(self):
        self.proxies_test_base(SPEC_TWO_HOST_TWO_FIELD_DICT["stations"][0], "host")

    def test_not_connected(self):
        self.m_device_proxy.side_effect = [DevFailed()]

        t_obs_future = station_observation_future.StationObservationFuture(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], "host"
        )
        self.assertFalse(t_obs_future.connected)

    def test_connected(self):
        t_obs_future = station_observation_future.StationObservationFuture(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], "host"
        )

        self.assertTrue(t_obs_future.connected)

    def test_start(self):
        m_add = mock.Mock()
        self.m_device_proxy.return_value = m_add
        t_obs_future = station_observation_future.StationObservationFuture(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], "host"
        )

        t_obs_future.start()
        m_add.add_observation.assert_called_with(
            dumps(t_obs_future._specification), wait=False
        )
