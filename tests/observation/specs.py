#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from json import loads

SPEC_SINGLE_HOST_SINGLE_FIELD_DICT = loads(
    """
        {
            "stations": [{
                "station": "cs002",
                "antenna_fields": [{
                    "observation_id": 12345,
                    "stop_time": "2106-02-07T00:00:00",
                    "antenna_field": "HBA0",
                    "antenna_set": "ALL",
                    "antenna_mask": [0,1,2,9],
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [10, 20, 30],
                        "pointing": {
                            "angle1": 1.5, "angle2": 0, "direction_type": "J2000"
                        }
                    }],
                    "tile_beam":
                    { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" },
                    "first_beamlet": 0
                }]
            }]
        }
    """
)

SPEC_TWO_HOST_TWO_FIELD_DICT = loads(
    """
        {
            "stations": [{
                "station": "cs002",
                "antenna_fields": [{
                    "observation_id": 12345,
                    "stop_time": "2106-02-07T00:00:00",
                    "antenna_field": "HBA0",
                    "antenna_set": "ALL",
                    "antenna_mask": [0,1,2,9],
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [10, 20, 30],
                        "pointing": {
                            "angle1": 1.5, "angle2": 0, "direction_type": "J2000"
                        }
                    }],
                    "tile_beam":
                    { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" },
                    "first_beamlet": 0
                },
                {
                    "observation_id": 12345,
                    "stop_time": "2106-02-07T00:00:00",
                    "antenna_field": "HBA0",
                    "antenna_set": "ALL",
                    "antenna_mask": [0,1,2,9],
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [10, 20, 30],
                        "pointing": {
                            "angle1": 1.5, "angle2": 0, "direction_type": "J2000"
                        }
                    }],
                    "tile_beam":
                    { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" },
                    "first_beamlet": 0
                }]
            },
            {
                "station": "cs003",
                "antenna_fields": [{
                    "observation_id": 12345,
                    "stop_time": "2106-02-07T00:00:00",
                    "antenna_field": "HBA0",
                    "antenna_set": "ALL",
                    "antenna_mask": [0,1,2,9],
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [10, 20, 30],
                        "pointing": {
                            "angle1": 1.5, "angle2": 0, "direction_type": "J2000"
                        }
                    }],
                    "tile_beam":
                    { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" },
                    "first_beamlet": 0
                },
                {
                    "observation_id": 12345,
                    "stop_time": "2106-02-07T00:00:00",
                    "antenna_field": "HBA1",
                    "antenna_set": "ALL",
                    "antenna_mask": [0,1,2,9],
                    "filter": "HBA_110_190",
                    "SAPs": [{
                        "subbands": [10, 20, 30],
                        "pointing": {
                            "angle1": 1.5, "angle2": 0, "direction_type": "J2000"
                        }
                    }],
                    "tile_beam":
                    { "angle1": 1.5, "angle2": 0, "direction_type": "J2000" },
                    "first_beamlet": 0
                }]
            }]
        }
    """
)
