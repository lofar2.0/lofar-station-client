#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-many-instance-attributes,duplicate-code

"""Statistics related package definitions"""

import struct
from datetime import datetime
from typing import TypeVar, Generic

import numpy

from ._base import _BasePacketHeader
from ._utils import get_bit_value

N_POL = 2
N_COMPLEX = 2

Subfields = TypeVar("Subfields")


class StatisticsHeader(Generic[Subfields], _BasePacketHeader):
    """Models a UDP packet from SDP.

    Packets are expected to be UDP payload only (so no Ethernet/IP/UDP headers).

    The following additional fields are exposed as properties & functions.

    subband_calibrated_flag    1 = subband data had subband calibration values applied,
                               0 = not.

    data_id                    bit field with payload information, encoding several
                               other properties.

    nof_signal_inputs          number of inputs that contributed to data in this packet.
    nof_bytes_per_statistics   word size of each statistic.
    nof_statistics_per_packet  number of statistic data points in the payload.

    integration_interval_raw   integration interval, in block periods.
    integration_interval     integration interval, in seconds.

    """

    def __init__(self, packet: bytes):
        self.subband_calibrated_flag: int = 0
        self.nof_signal_inputs: int = 0
        self.nof_statistics_per_packet: int = 0
        self.nof_bytes_per_statistic: int = 0
        self.data_id: Subfields = None
        self.data_id_raw: int = 0
        self.integration_interval_raw: int = 0

        super().__init__(packet)

    # format string for the header, see unpack below
    HEADER_FORMAT = ">cBL HH xBH LB BHHQ"
    HEADER_SIZE = struct.calcsize(HEADER_FORMAT)

    @property
    def raw(self) -> bytes:
        return self.packet

    def unpack(self):
        """Unpack the packet into properties of this object."""

        # unpack fields
        try:
            (
                self.marker_raw,
                self.version_id,
                self.observation_id,
                self.station_info,
                self.source_info,
                integration_interval_hi,
                integration_interval_lo,
                self.data_id_raw,
                self.nof_signal_inputs,
                self.nof_bytes_per_statistic,
                self.nof_statistics_per_packet,
                self.block_period_raw,
                self.block_serial_number,
            ) = struct.unpack(self.HEADER_FORMAT, self.packet[: self.HEADER_SIZE])
        except struct.error as ex:
            raise ValueError("Error parsing statistics packet") from ex

        self.integration_interval_raw = (
            integration_interval_hi << 16
        ) + integration_interval_lo
        # unpack the fields we just updated

    def valid_markers(self):
        return [b"S", b"B", b"X"]

    def unpack_source_info(self):
        """Unpack the source_info field into properties of this object."""
        super().unpack_source_info()
        self.subband_calibrated_flag = get_bit_value(self.source_info, 8) != 0

    def expected_size(self) -> int:
        """Determine packet size (header + payload) from the header"""

        return (
            self.HEADER_SIZE
            + self.nof_statistics_per_packet * self.nof_bytes_per_statistic
        )

    @property
    def integration_interval(self) -> float:
        """Returns the integration interval, in seconds."""

        # Translate to seconds using the block period
        return self.integration_interval_raw * self.block_period

    def __iter__(self):
        """Return all the header fields as a dict."""
        for key, value in super().__iter__():
            if key == "source_info":
                yield key, {
                    **value,
                    "subband_calibrated_flag": self.subband_calibrated_flag,
                }
            else:
                yield key, value
        yield "data_id", {**dict(self.data_id), "_raw": self.data_id_raw}
        yield "integration_interval_raw", self.integration_interval_raw
        yield "integration_interval", self.integration_interval
        yield "nof_signal_inputs", self.nof_signal_inputs
        yield "nof_bytes_per_statistic", self.nof_bytes_per_statistic
        yield "nof_statistics_per_packet", self.nof_statistics_per_packet


class StatisticsPacket:
    """Models a statistics UDP packet from SDP.

    Packets are expected to be UDP payload only (so no Ethernet/IP/UDP headers).
    """

    def __init__(self, header: StatisticsHeader, payload: bytes):
        self._header: StatisticsHeader = header
        self.payload_data: bytes = payload

    def __new__(cls, header: StatisticsHeader, packet_data: bytes):
        if cls == StatisticsPacket:
            return PACKET_CLASS_FOR_MARKER[header.marker_raw](header, packet_data)

        return super().__new__(cls)

    def __iter__(self):
        yield from self.header
        yield "payload", self.payload()

    @staticmethod
    def parse_packet(packet_data: bytes) -> "StatisticsPacket":
        """Parses bytes into the corresponding StatisticsPacket"""
        header = StatisticsHeader(packet_data[0 : StatisticsHeader.HEADER_SIZE])
        return StatisticsPacket(header, packet_data[StatisticsHeader.HEADER_SIZE :])

    @property
    def raw(self) -> bytes:
        """Returns the raw packet bytes"""
        return self._header.raw + self.payload_data

    @property
    def header(self) -> StatisticsHeader:
        """Returns the packet header"""
        return self._header

    @property
    def timestamp(self) -> datetime:
        """Returns the packet timestamp"""
        return self.header.timestamp

    def payload(self, signed=False) -> numpy.array:
        """The payload of this packet, as a linear array."""

        # derive which and how many elements to read from the packet header
        bytecount_to_unsigned_struct_type = (
            {1: "b", 2: "h", 4: "i", 8: "q"}
            if signed
            else {1: "B", 2: "H", 4: "I", 8: "Q"}
        )
        format_str = (
            f">{self.header.nof_statistics_per_packet}"
            f"{bytecount_to_unsigned_struct_type[self.header.nof_bytes_per_statistic]}"
        )

        return numpy.array(
            struct.unpack_from(
                format_str,
                self.payload_data,
            )
        )


class SSTPacket(StatisticsPacket):
    """Models an SST statistics UDP packet from SDP.

    The following fields are exposed as properties & functions.

    signal_input_index:
        input (antenna polarisation) index for which this packet contains statistics

    payload[nof_statistics_per_packet]:
        SST statistics, an array of amplitudes per subband.
    """

    #  pylint: disable=too-few-public-methods
    class SSTHeaderSubFields:
        """SST packet header subfields"""

        def __init__(self, data_id):
            self.signal_input_index = get_bit_value(data_id, 0, 7)

        def __iter__(self):
            yield "signal_input_index", self.signal_input_index

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._header.data_id = SSTPacket.SSTHeaderSubFields(self._header.data_id_raw)

    @property
    def header(self) -> StatisticsHeader[SSTHeaderSubFields]:
        """Returns the packet header"""
        return super().header

    def payload(self, signed=True):
        """Returns the packet payload"""
        return super().payload(signed=False)


class XSTPacket(StatisticsPacket):
    """Models an XST statistics UDP packet from SDP.

    The following fields are exposed as properties & functions.

    subband_index:
        subband number for which this packet contains statistics.
    first_baseline:
        first antenna pair for which this packet contains statistics.

    payload[nof_signal_inputs][nof_signal_inputs] the baselines, starting from
    first_baseline
    """

    #  pylint: disable=too-few-public-methods
    class XstHeaderSubFields:
        """XST packet header subfields"""

        def __init__(self, data_id):
            self.subband_index = get_bit_value(data_id, 16, 24)
            self.first_baseline = (
                get_bit_value(data_id, 8, 15),
                get_bit_value(data_id, 0, 7),
            )

        def __iter__(self):
            yield "subband_index", self.subband_index
            yield "first_baseline", self.first_baseline

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._header.data_id = XSTPacket.XstHeaderSubFields(self._header.data_id_raw)

    @property
    def header(self) -> StatisticsHeader[XstHeaderSubFields]:
        """Returns the packet header"""
        return super().header

    def payload(self, signed=True):
        """Returns the packet payload"""
        return super().payload(signed=True)


class BSTPacket(StatisticsPacket):
    """Models an BST statistics UDP packet from SDP.

    The following fields are exposed as properties & functions.

    beamlet_index:
        the number of the beamlet for which this packet holds statistics.
    """

    #  pylint: disable=too-few-public-methods
    class BstHeaderSubFields:
        """BST packet header subfields"""

        def __init__(self, data_id):
            self.beamlet_index = get_bit_value(data_id, 0, 15)

        def __iter__(self):
            yield "beamlet_index", self.beamlet_index

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._header.data_id = BSTPacket.BstHeaderSubFields(self._header.data_id_raw)

    @property
    def header(self) -> StatisticsHeader[BstHeaderSubFields]:
        """Returns the packet header"""
        return super().header

    def payload(self, signed=True):
        # We have signed values, per beamlet in pairs
        # for each polarisation.
        return super().payload(signed=True).reshape(-1, N_POL)


# Which class to use for which marker.
#
# NB: Python does not allow us to register from inside the class,
#     as we cannot reference the class during its construction.
PACKET_CLASS_FOR_MARKER = {
    b"S": SSTPacket,
    b"B": BSTPacket,
    b"X": XSTPacket,
}
