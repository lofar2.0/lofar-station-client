====================================================
Welcome to the documentation of Lofar Station Client
====================================================

..
    To define more variables see rst_epilog generation in conf.py

Documentation for version: |version|

Contents:

.. toctree::
   :maxdepth: 2

   source_documentation/index
