#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# too-few-public-methods
# pylint: disable=R0903

"""
Models the structure of an HDF statistics file.
"""
import inspect
from typing import Dict

from numpy import ndarray

from lofar_station_client.file_access import attribute


class StatisticsFileHeader:
    """
    Pythonic representation of the HDF statistics file header written by the statistics
    writer.
    """

    station_name: str = attribute(optional=True)
    """ Name of the station the statistics where recorded from """

    station_version: str = attribute()
    """ Lofar Station Control version """

    writer_version: str = attribute()
    """ Statistics Writer software version """

    mode: str = attribute()
    """ Mode of the statistics writer (SST,XST,BST) """

    antennafield_device: str = attribute(optional=True)
    """ Name of the antennafield device """

    antenna_names: [str] = attribute(optional=True)
    """ Antenna names. """

    antenna_type: str = attribute(optional=True)
    """ The type of antenna in this field (LBA or HBA). """

    antenna_status: [str] = attribute(optional=True)
    """ The status of each antenna (BROKEN/OK/etc), as a string. """

    antenna_usage_mask: [bool] = attribute(optional=True)
    """ Whether each antenna would have been used. """

    frequency_band: str = attribute(optional=True)
    """ For which band the RECV and SDP are configured, f.e. 'LBA_10_90'. """

    clock: float = attribute(optional=True)
    """ Sample clock, in Hz. """

    antenna_reference_itrf: [str] = attribute(optional=True)
    """ Absolute reference position of each tile, in ITRF (XYZ) """

    fpga_firmware_version: [str] = attribute(optional=True)
    fpga_hardware_version: [str] = attribute(optional=True)

    rcu_pcb_id: [int] = attribute(optional=True)
    rcu_pcb_version: [str] = attribute(optional=True)

    subband_frequencies: [float] = attribute(optional=True)
    """ Frequencies of all 512 subbands. """

    dithering_on: [bool] = attribute(optional=True)
    """ Whether the RCU is dithering the signal. """

    dithering_frequency: [float] = attribute(optional=True)
    """ Dithering frequency. """

    digitalbeam_pointing_directions: [str] = attribute(optional=True)
    """ Pointing direction of each beamlet in SDP, f.e. 'J2000 (0deg, 0deg)'. """

    digitalbeam_tracking_enabled: bool = attribute(optional=True)
    """ Whether the SDP beam weights are periodically updated. """

    tilebeam_pointing_directions: [str] = attribute(optional=True)
    """ Pointing direction of each HBA tile, f.e. 'J2000 (0deg, 0deg)'. """

    tilebeam_tracking_enabled: bool = attribute(optional=True)
    """ Whether the beam weights are periodically updated on the tile. """

    tile_element_power: [int] = attribute(optional=True)
    """ Whether each element in the tile was powered on. """

    def __eq__(self, other):
        for attr in [
            a[0]
            for a in inspect.getmembers(__class__, lambda a: not inspect.isroutine(a))
            if not a[0].startswith("_")
        ]:
            if not hasattr(self, attr) or not hasattr(other, attr):
                return False
            if getattr(self, attr) != getattr(other, attr):
                return False

        return True


class StatisticsSet(ndarray):
    """
    Pythonic representation of a dataset within an HDF statistics file for statistics.

    Contains the data array, and key packet information.
    """

    timestamp: str = attribute(optional=True)
    integration_time: float = attribute(optional=True)


class StatisticsDataFile(Dict[str, ndarray], StatisticsFileHeader):
    """
    Pythonic representation of the HDF statistics file written by the statistics
    writer.
    """


class BstStatisticsSet(StatisticsSet):
    """
    Pythonic representation of a dataset within an HDF statistics file for BSTs.
    """

    subbands: [int] = attribute()


class BstStatisticsDataFile(Dict[str, BstStatisticsSet]):
    """
    Pythonic representation of the HDF statistics file for BSTs written by
    the statistics writer.
    """


class SstStatisticsSet(StatisticsSet):
    """
    Pythonic representation of a dataset within an HDF statistics file for SSTs.
    """

    subbands: [int] = attribute()


class SstStatisticsDataFile(Dict[str, SstStatisticsSet], StatisticsFileHeader):
    """
    Pythonic representation of the HDF statistics file for SSTs written by the
    statistics writer.
    """


class XstStatisticsSet(StatisticsSet):
    """
    Pythonic representation of a dataset within an HDF statistics file for XSTs.
    """

    subband: int = attribute()


class XstStatisticsDataFile(Dict[str, XstStatisticsSet], StatisticsFileHeader):
    """
    Pythonic representation of the HDF statistics file for XSTs written by the
    statistics writer.
    """
