#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0


"""Reader and parser of statistics files"""

# unused-import, too-many-instance-attributes, too-few-public-methods,
# global-variable-undefined, broad-except
# pylint: disable=W0611, R0902, R0903, W0601, W0703

import argparse
import datetime
import logging
import os
import sys  # noqa: F401

import numpy
import psutil
import pytz

from lofar_station_client.file_access import read_hdf5
from lofar_station_client.statistics.statistics_data import (
    StatisticsDataFile,
    StatisticsFileHeader,
)

process = psutil.Process(os.getpid())

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("hdf5_explorer")
logger.setLevel(logging.DEBUG)


def timeit(method):
    """
    Simple decorator function to log time, function and process memory usage
    """

    def timed(*args, **kw):
        time_s = datetime.datetime.now()
        result = method(*args, **kw)
        time_e = datetime.datetime.now()

        size_mb = process.memory_info().rss / 1024 / 1024
        size_mb_str = "{0:,}".format(  # pylint: disable=consider-using-f-string
            round(size_mb, 2)
        )

        logger.debug(
            "Time taken = %s,  %s  ,size = %s MB",
            time_e - time_s,
            method.__name__,
            size_mb_str,
        )
        return result

    return timed


class StatisticsParser:
    """
    This class goes through the file and creates a list
    of all statistics in the file it is given
    """

    def __init__(self):
        # list of all statistics
        self.statistics = []

        # dict of all statistics, allows for easier access.
        self.statistics_dict = {}

        # header of the file(s)
        self.file_header: StatisticsFileHeader = None

        # for setting the range of times to parse.
        # Initialise with the build in minimum and maximum values
        self.start_time = datetime.datetime.min.replace(tzinfo=pytz.UTC)
        self.end_time = datetime.datetime.max.replace(tzinfo=pytz.UTC)

    def set_start_time(self, start_time):
        """
        set the lowest statistics timestamp to store
        """
        self.start_time = datetime.datetime.fromisoformat(start_time)

    def set_end_time(self, end_time):
        """
        set the highest statistics timestamp to store
        """
        self.end_time = datetime.datetime.fromisoformat(end_time)

    @timeit
    def parse_file(self, files):
        """
        This function opens and parses the statistics HDF5 file
        and adds it to self.statistics.
        """

        # if its just a single file the type could be string
        if isinstance(files, str):
            files = [files]

        for file in files:
            hdf5_file = read_hdf5(file, StatisticsDataFile)

            # go through all the groups
            logger.debug("Parsing hdf5 statistics file")

            with hdf5_file as statistic_data:
                hdf5_file.load(statistic_data)
                if self.file_header is None:
                    self.file_header = statistic_data
                elif not StatisticsFileHeader.__eq__(self.file_header, statistic_data):
                    raise ValueError(
                        "Cannot read statistic files with different headers at the "
                        "same time"
                    )

                for group_key, statistic in statistic_data.items():
                    try:
                        # extract the timestamp and convert to datetime
                        statistic_time = datetime.datetime.fromisoformat(
                            statistic.timestamp
                        )

                        # check if the timestamp is before the start time
                        if statistic_time < self.start_time:
                            continue

                        # check if the timestamp is after the end times
                        if statistic_time > self.end_time:
                            # Exit, we're done
                            logger.debug("Parsed %s statistics", len(self.statistics))
                            return

                        # append to the statistics list
                        hdf5_file.load(statistic)
                        self.statistics.append(statistic)
                        self.statistics_dict[
                            statistic_time.isoformat(timespec="milliseconds")
                        ] = statistic

                    except Exception:
                        # B001 Do not use bare `except:`, it also catches unexpected
                        # events like memory errors, interrupts, system exit
                        logger.exception(
                            "Encountered an error while parsing statistic. \
                            Skipped: %s",
                            group_key,
                        )

                logger.debug("Parsed %s statistics", len(self.statistics))

    @timeit
    def collect_values(self):
        """ "
        Collects all of the statistics values in to a single giant numpy array
        Uses a lot more memory (Basically double since
        the values make up the bulk of memory)
        """
        lst = [i.sst_values for i in self.statistics]
        value_array = numpy.stack(lst)
        return value_array

    def sort_by_timestamp(self):
        """
        Ensures the statistics are correctly sorted.
        In case files arent given in sequential order.
        """
        self.statistics.sort(key=lambda r: r.timestamp)

    def get_statistic(self, timestamp):
        """
        Returns a statistic object based on the timestamp given.
        """
        for i in self.statistics:
            if i.timestamp == timestamp:
                return i

        raise ValueError(
            f"No statistic with timestamp {timestamp} found, \
            make sure to use the isoformat"
        )

    def list_statistics(self):
        """
        Returns a list of all statistics
        """
        return self.statistics_dict.keys()

    def get_statistics_count(self):
        """
        Simply returns the amount of statistics
        """
        return len(self.statistics)


def parse_arguments():
    """
    This function parses the input arguments.
    """

    parser = argparse.ArgumentParser(description="Select a file to explore")
    parser.add_argument(
        "--files",
        type=str,
        nargs="+",
        required=True,
        help="the name and path of the files, takes one or more files",
    )
    parser.add_argument(
        "--start_time",
        type=str,
        required=True,
        help="lowest timestamp to process (uses isoformat, ex: 2021-10-04T07:50"
        ":08.937+00:00)",
    )
    parser.add_argument(
        "--end_time",
        type=str,
        required=True,
        help="highest timestamp to process (usesisoformat, ex: 2021-10-04T07:50"
        ":08.937+00:00)",
    )

    args = parser.parse_args()
    files = args.files
    end_time = args.end_time
    start_time = args.start_time

    return files, end_time, start_time


def setup_stat_parser():
    """
    This function takes care of setting up the statistics parser
    with the end_time, start_time and files arguments
    """

    files, end_time, start_time = parse_arguments()

    # create the parser
    stat_parser = StatisticsParser()

    # set the correct time ranges
    if end_time is not None:
        stat_parser.set_end_time(end_time)
    if start_time is not None:
        stat_parser.set_start_time(start_time)

    # parse all the files
    stat_parser.parse_file(files)

    # for good measure sort all the statistics by timestamp.
    # Useful when multiple files are given out of order
    stat_parser.sort_by_timestamp()

    return stat_parser


def main():
    """Reader main method"""

    # set up everything
    stat_parser = setup_stat_parser()

    # get a single numpy array of all the statistics stored.
    array = stat_parser.collect_values()
    logger.debug(
        "Collected the statistics values of %s \
        statistics in to one gaint array of shape: %s \
        and type: %s",
        stat_parser.get_statistics_count(),
        array.shape,
        array.dtype,
    )

    # Get a list of all the statistics timestamps we have
    statistics = stat_parser.list_statistics()
    logger.debug("These are all the statistics parsed: %s", statistics)
