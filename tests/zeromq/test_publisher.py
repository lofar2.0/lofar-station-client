# Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime
import json
import logging
import queue
import time
from importlib.resources import files
from multiprocessing.sharedctypes import Value
from ctypes import c_int8
from threading import Thread
from typing import Any, Dict

import zmq
from zmq.utils.monitor import recv_monitor_message
from timeout_decorator import timeout_decorator

from lofar_station_client.zeromq.publisher import ZeroMQPublisher

from unittest import mock
from tests import base

logger = logging.getLogger()


class TestPublisher(base.TestCase):

    DEFAULT_PUBLISH_ADDRESS = "tcp://*:6001"
    DEFAULT_SUBSCRIBE_ADDRESS = "tcp://127.0.0.1:6001"

    @staticmethod
    def event_monitor_loop(monitor: zmq.Socket, trigger: Value):
        """Loop on monitor socket to count number of subscribers

        :param monitor: zmq monit socket, use `get_monitor_socket()`
        :param trigger: multiprocessing shared value, must be incrementable / number
        """
        while monitor.poll():
            evt: Dict[str, Any] = {}
            mon_evt = recv_monitor_message(monitor)
            evt.update(mon_evt)
            evt["description"] = evt["event"]
            logger.warning(f"Event: {evt}")
            if evt["event"] == zmq.EVENT_HANDSHAKE_SUCCEEDED:
                logger.info("Setting connected to true")
                trigger.value += 1
            elif evt["event"] == zmq.EVENT_DISCONNECTED:
                logger.info("Dropping connection")
                trigger.value -= 1
            elif evt["event"] == zmq.EVENT_MONITOR_STOPPED:
                break
        monitor.close()

    @staticmethod
    def create_event_monitor(monitor: zmq.Socket, trigger: Value):
        """Create a thread that uses an event monitor socket"""
        t_monitor = Thread(
            target=TestPublisher.event_monitor_loop, args=(monitor, trigger)
        )
        t_monitor.start()
        return t_monitor

    @staticmethod
    def wait_for_start(publisher: ZeroMQPublisher):
        """Spin until publisher is running"""
        while not publisher.is_running:
            logger.info("Waiting for publisher thread to start..")
            time.sleep(0.1)

    @staticmethod
    def load_test_json():
        """Load test_configdb into memory from disc"""
        file_path = files(__package__).joinpath("test_configdb.json")
        with file_path.open() as _file:
            return json.dumps(json.load(_file))

    def test_contstruct_bind_uri(self):
        """Test that helper function creates proper strings"""

        self.assertEqual(
            "tcp://0.0.0.0:1624",
            ZeroMQPublisher.contstruct_bind_uri("tcp", "0.0.0.0", 1624),
        )

        self.assertEqual(
            "udp://0.0.0.0:1624",
            ZeroMQPublisher.contstruct_bind_uri("udp", "0.0.0.0", "1624"),
        )

    @timeout_decorator.timeout(5)
    def test_topic_bytearray(self):
        """Pass a list of topics as bytearray"""
        t_topics = [b"A", b"B"]

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, t_topics) as t_publisher:
            self.assertListEqual(t_topics, t_publisher._topics)

    @timeout_decorator.timeout(5)
    def test_topic_str(self):
        """Pass a list of topics as str"""
        t_topics = ["A", "B"]

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, t_topics) as t_publisher:
            self.assertNotEqual(t_topics, t_publisher._topics)

            t_topics = [b"A", b"B"]

            self.assertListEqual(t_topics, t_publisher._topics)

    @timeout_decorator.timeout(5)
    def test_start_stop(self):
        """Test the startup and shutdown sequence"""
        t_topic = b"A"
        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as t_publisher:
            self.wait_for_start(t_publisher)

            self.assertTrue(t_publisher.is_running)
            self.assertFalse(t_publisher.is_stopping)
            self.assertFalse(t_publisher.is_done)

            t_publisher.shutdown()

            self.assertTrue(t_publisher.is_stopping)

            while not t_publisher.is_done:
                logger.info("Waiting for publisher thread to stop..")
                time.sleep(0.1)

            self.assertIsNone(t_publisher.get_exception())
            self.assertFalse(t_publisher.is_running)
            self.assertTrue(t_publisher.is_done)

    @timeout_decorator.timeout(5)
    def test_publish(self):
        """Test publishing a message and having a subscriber receive it"""
        t_msg = "test"
        t_topic = b"A"

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as t_publisher:
            self.wait_for_start(t_publisher)

            ctx = zmq.Context.instance()

            t_connected = Value(c_int8, 0, lock=False)

            self.create_event_monitor(
                t_publisher._publisher.get_monitor_socket(), t_connected
            )

            subscribe = ctx.socket(zmq.SUB)
            subscribe.connect(self.DEFAULT_SUBSCRIBE_ADDRESS)
            subscribe.setsockopt(zmq.SUBSCRIBE, t_topic)

            while not t_connected.value >= 1:
                logger.info("Waiting for topic subscription..")
                time.sleep(0.1)

            for _ in range(
                0, 5
            ):  # check against accidental shutdown after first message
                t_publisher.send(t_msg)
                msg = subscribe.recv_multipart()
                self.assertIsInstance(datetime.fromisoformat(msg[1].decode()), datetime)
                self.assertEqual(t_msg.encode(), msg[2])

            subscribe.close()

            while not t_connected.value == 0:
                logger.info("Waiting for subscriber to disconnect")
                time.sleep(0.1)

            self.assertTrue(t_publisher.is_running)
            self.assertFalse(t_publisher.is_stopping)
            self.assertFalse(t_publisher.is_done)

    def test_publish_huge_message_multi_subscriber(self):
        test_data = self.load_test_json()
        t_topic = b"A"

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as t_publisher:
            self.wait_for_start(t_publisher)

            ctx = zmq.Context.instance()

            t_connected = Value(c_int8, 0, lock=False)

            self.create_event_monitor(
                t_publisher._publisher.get_monitor_socket(), t_connected
            )

            subscribers = []
            for _ in range(0, 2):
                subscribe = ctx.socket(zmq.SUB)
                subscribe.connect(self.DEFAULT_SUBSCRIBE_ADDRESS)
                subscribe.setsockopt(zmq.SUBSCRIBE, t_topic)
                subscribers.append(subscribe)

            while not t_connected.value >= 2:
                logger.info("Waiting for topic subscriptions..")
                time.sleep(0.1)

            t_publisher.send(test_data)
            for i in range(0, 2):
                msg = subscribers[i].recv_multipart()
                self.assertIsInstance(datetime.fromisoformat(msg[1].decode()), datetime)
                self.assertEqual(test_data.encode(), msg[2])

    def test_callback(self):
        """Test that triggering done callbacks works"""

        t_topic = b"A"

        with ZeroMQPublisher(self.DEFAULT_PUBLISH_ADDRESS, [t_topic]) as t_publisher:
            self.wait_for_start(t_publisher)

            t_cb = mock.Mock()

            t_publisher.register_callback(t_cb)

            t_publisher.shutdown()

            while not t_publisher.is_stopping or not t_publisher.is_done:
                logger.info("Waiting for publisher thread to stop..")
                time.sleep(0.1)

            t_cb.assert_called_once()

    def test_queue(self):
        """Test queuing of messages and full exception"""
        t_queue_size = 10
        t_topic = b"A"
        t_publisher = ZeroMQPublisher(
            bind_uri=self.DEFAULT_PUBLISH_ADDRESS,
            topics=[t_topic],
            queue_size=t_queue_size,
        )
        t_publisher.shutdown()

        while not t_publisher.is_stopping or not t_publisher.is_done:
            logger.info("Waiting for publisher thread to stop..")
            time.sleep(0.1)

        self.assertEqual(t_queue_size, t_publisher.queue_size)

        for i in range(1, t_queue_size + 1):
            t_publisher.send("hello")
            self.assertEqual(i, t_publisher.queue_fill)

        self.assertRaises(queue.Full, t_publisher.send, "hello")
