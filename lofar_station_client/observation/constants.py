#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Constants used across observation management functionality"""

OBSERVATION_CONTROL_DEVICE_NAME = "STAT/ObservationControl/1"

OBSERVATION_FIELD_DEVICE_NAME = "STAT/ObservationField"
