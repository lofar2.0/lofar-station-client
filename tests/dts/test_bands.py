# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import numpy as np

from lofar_station_client.dts import bands

from tests import base


class DTSBandsTest(base.TestCase):
    def test_get_band_name_and_subband_index_for_frequency_single(self):
        """Test automatic single value to list conversion in sbi_for_freq"""

        result = bands.get_band_name_and_subband_index_for_frequency(138.1e6)

        self.assertEqual(result[0], "HB1")
        self.assertEqual(result[1], 317.0)

        # Access ndarray element directly
        self.assertEqual(result[0][0], "HB1")
        self.assertEqual(result[1][0], 317.0)

    def test_get_band_name_for_frequency(self):
        result = bands.get_band_name_for_frequency(138.1e6)
        self.assertEqual("HB1", result)

    def test_get_band_name_for_frequency_not_exist(self):
        result = bands.get_band_name_for_frequency(998.1e6)
        self.assertEqual("", result)

    def test_get_band_name_for_frequency_custom_bandlims(self):
        t_bandlims = {"LB": [0, 900e6], "HB1": [900e6, 1000e6], "HB2": [1000e6, 1300e6]}
        result = bands.get_band_name_for_frequency(998.1e6, t_bandlims)
        self.assertEqual("HB1", result)

    def test_get_band_name_and_subband_index_for_frequency(self):
        """Test code for get_band_name_and_subband_index_for_frequency"""

        input_frequencies = [138.1e6, 137.9e6, 0.1, 0, 195312.5, 234e6]
        expected_band_names = ["HB1", "HB1", "LB", "LB", "LB", "HB2"]
        expected_subband_index = np.array([317.0, 318.0, 0.0, 0.0, 1.0, 174.0])
        (
            actual_band_names,
            actual_subband_index,
        ) = bands.get_band_name_and_subband_index_for_frequency(input_frequencies)

        np.testing.assert_array_equal(expected_band_names, actual_band_names)
        np.testing.assert_array_equal(expected_subband_index, actual_subband_index)

    def test_get_frequency_for_band_name_and_subband_index(self):
        """Test code for get_frequency_for_band_name_and_subband_index"""

        for input_band_name, input_subband_index, expected_frequencies in zip(
            ["HB1", "HB1", "LB", "LB", "LB", "HB2", "LB1"],
            [195, 194, 0, 0, 1, 174, 1],
            [161914062.5, 162109375.0, 0.0, 0.0, 195312.5, 233984375, 195312.5],
        ):
            actual_frequencies = bands.get_frequency_for_band_name_and_subband_index(
                input_band_name, input_subband_index
            )

            self.assertEqual(expected_frequencies, actual_frequencies)

    def test_get_frequency_for_band_name_and_subband_index_except(self):
        """Test retrieving frequency for non-existing band raises exception"""

        self.assertRaises(
            ValueError,
            bands.get_frequency_for_band_name_and_subband_index,
            "not-exists",
            0,
        )

    def test_get_frequency_for_band_name_hb1(self):
        """HB1 band should return frequencies in descending order"""
        frequencies = bands.get_frequency_for_band_name("HB1")

        previous_freq = frequencies[0]
        for x in frequencies[1:]:
            self.assertLess(x, previous_freq)

    def test_get_frequency_for_band_name_hb2(self):
        """HB2 band should return frequencies in ascending order"""
        frequencies = bands.get_frequency_for_band_name("HB2")

        previous_freq = frequencies[0]
        for x in frequencies[1:]:
            self.assertGreater(x, previous_freq)

    def test_get_frequency_for_band_name_lb(self):
        """All bands except HB1 should return frequencies in ascending order"""
        frequencies = bands.get_frequency_for_band_name("LB1")

        previous_freq = frequencies[0]
        for x in frequencies[1:]:
            self.assertGreater(x, previous_freq)

    def test_get_band_names(self):
        pass
