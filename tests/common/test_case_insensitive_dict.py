#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0
from enum import Enum

from lofar_station_client.common import CaseInsensitiveDict
from lofar_station_client.common import CaseInsensitiveString
from lofar_station_client.common import ReversibleKeysView
from tests import base


class TestCaseInsensitiveDict(base.TestCase):
    def test_set_get_item(self):
        """Get and set an item with different casing"""

        t_value = "VALUE"
        t_key = "KEY"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value

        self.assertEqual(t_value, t_dict[t_key.lower()])
        self.assertEqual(t_value, t_dict.get(t_key.lower()))

    def test_set_overwrite(self):
        """Overwrite a previous element with different casing"""

        t_value = "VALUE"
        t_key = "KEY"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value
        t_dict[t_key.lower()] = t_value.lower()

        self.assertEqual(t_value.lower(), t_dict[t_key])
        self.assertEqual(t_value.lower(), t_dict.get(t_key))

    class ConstructTestEnum(Enum):
        DICT = "dict"
        ITER = "iter"
        KWARGS = "kwargs"

    def construct_base(self, test_type: ConstructTestEnum):
        t_key1 = "KEY1"
        t_key2 = "key2"
        t_value1 = 123
        t_value2 = 456
        t_mapping = {t_key1: t_value1, t_key2: t_value2}

        t_dict = CaseInsensitiveDict()
        if test_type is self.ConstructTestEnum.DICT:
            t_dict = CaseInsensitiveDict(t_mapping)
        elif test_type is self.ConstructTestEnum.ITER:
            t_dict = CaseInsensitiveDict(t_mapping.items())
        elif test_type is self.ConstructTestEnum.KWARGS:
            t_dict = CaseInsensitiveDict(KEY1=t_value1, key2=t_value2)

        self.assertEqual(t_value1, t_dict[t_key1.lower()])
        self.assertEqual(t_value2, t_dict.get(t_key2.upper()))

    def test_construct_mapping(self):
        self.construct_base(self.ConstructTestEnum.DICT)

    def test_construct_iterable(self):
        self.construct_base(self.ConstructTestEnum.ITER)

    def test_construct_kwargs(self):
        self.construct_base(self.ConstructTestEnum.KWARGS)

    def test_setdefault(self):
        t_key = "KEY"
        t_value = "value"
        t_dict = CaseInsensitiveDict()

        t_dict.setdefault(t_key, t_value)

        self.assertIn(t_key.lower(), t_dict.keys())

        for key in t_dict.keys():
            self.assertEqual(t_key, key)
            self.assertIsInstance(key, CaseInsensitiveString)

    def test_keys(self):
        t_key = "KEY"
        t_value = "value"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value

        self.assertIn(t_key.lower(), t_dict.keys())
        self.assertIsInstance(t_dict.keys(), ReversibleKeysView)

        for key in t_dict.keys():
            self.assertEqual(t_key, key)
            self.assertIsInstance(key, CaseInsensitiveString)

    def test_items(self):
        t_key = "KEY"
        t_value = "VALUE"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value

        for key, value in t_dict.items():
            self.assertEqual(t_key, key)
            self.assertIsInstance(key, CaseInsensitiveString)
            self.assertNotEqual(t_value.casefold(), value)

    def test_values(self):
        t_key = "KEY"
        t_value = "VALUE"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value

        for value in t_dict.values():
            self.assertEqual(t_value, value)
            self.assertIsInstance(value, str)

    def test_in(self):
        t_key = "KEY"
        t_value = "VALUE"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key] = t_value

        self.assertTrue(t_key.lower() in t_dict)

    def test_reverse(self):
        t_key1 = "KEY1"
        t_key2 = "KEY2"
        t_value = "VALUE"
        t_dict = CaseInsensitiveDict()

        t_dict[t_key1] = t_value
        t_dict[t_key2] = t_value

        forward = []
        for key, _ in t_dict.items():
            forward.append(key)

        backward = []
        for key, _ in reversed(t_dict.items()):
            backward.append(key)

        self.assertEqual(forward[0], backward[1])
        self.assertEqual(forward[1], backward[0])

        backward = []
        for key in reversed(t_dict.keys()):
            backward.append(key)

        self.assertEqual(forward[0], backward[1])
        self.assertEqual(forward[1], backward[0])

        forward = []
        for item in t_dict.values():
            forward.append(item)

        backward = []
        for value in reversed(t_dict.values()):
            backward.append(value)

        self.assertEqual(forward[0], backward[1])
        self.assertEqual(forward[1], backward[0])
