# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from lofar_station_client.parsing import parse

from tests import base


class ParseTest(base.TestCase):
    def test_validate_type(self):
        self.assertTrue(parse.validate_type("", [str]))

    def test_validate_type_invalid(self):
        self.assertFalse(parse.validate_type("", [int, None]))

    def test_validate_types(self):
        _validate_valid = [
            ("", [str], "test"),
            (1234, [int, None], "test"),
            (None, [None], "empty"),
        ]

        parse.validate_types(_validate_valid)
