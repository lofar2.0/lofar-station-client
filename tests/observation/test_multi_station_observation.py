#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

from typing import List, Callable
from unittest import mock
import concurrent.futures

import lofar_station_client
from lofar_station_client.observation import multi_station_observation

from tests import base
from tests.observation.specs import (
    SPEC_SINGLE_HOST_SINGLE_FIELD_DICT,
    SPEC_TWO_HOST_TWO_FIELD_DICT,
)


class TestMultiStationObservation(base.TestCase):
    def setUp(self):
        proxy_patcher = mock.patch.object(
            lofar_station_client.observation.multi_station_observation,
            "StationObservationFuture",
            autospec=True,
        )
        self.m_station_obs_future = proxy_patcher.start()
        self.addCleanup(proxy_patcher.stop)

    def configure_obs_future_mock(
        self, host: str, is_running: any = True, connected: bool = True
    ):
        """Configure StationObservationFuture mock and return it"""

        m_obs_future = mock.Mock()

        future_start_stop = concurrent.futures.Future()
        future_start_stop.set_result(True)

        future_running = concurrent.futures.Future()
        future_running.set_result(is_running)

        m_obs_future.start.return_value = future_start_stop
        m_obs_future.stop.return_value = future_start_stop
        type(m_obs_future).is_running = mock.PropertyMock(return_value=future_running)
        type(m_obs_future).connected = mock.PropertyMock(return_value=connected)
        type(m_obs_future).get_control_poxy = mock.PropertyMock(
            return_value="A_real_control_proxy"
        )

        type(m_obs_future).observations = mock.PropertyMock(
            return_value="A_real_observation_proxy1"
        )

        type(m_obs_future).host = mock.PropertyMock(return_value=host)

        return m_obs_future

    @mock.patch.object(multi_station_observation, "StationObservationFuture")
    def test_construct_called_correct_arguments(self, m_station_obs_future):
        m_host = "host1"
        multi_station_observation.MultiStationObservation(
            specifications=SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, hosts=[m_host]
        )

        m_station_obs_future.assert_called_once_with(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT["stations"][0], m_host
        )

    def test_create_station_object(self):
        """Test construction of MultiStationObs creates appropriate child objects"""
        m_host = "host1"

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, hosts=[m_host]
        )

        self.assertEqual(1, len(t_multi_observation._stations))

    def test_create_multiple_station_objects(self):
        """"""
        m_hosts = ["host1", "host2"]

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=SPEC_TWO_HOST_TWO_FIELD_DICT, hosts=m_hosts
        )

        self.assertEqual(2, len(t_multi_observation._stations))

    def test_create_station_object_error(self):
        """"""
        m_host = ["host1"]

        self.assertRaises(
            ValueError,
            multi_station_observation.MultiStationObservation,
            specifications=SPEC_TWO_HOST_TWO_FIELD_DICT,
            hosts=m_host,
        )

    def test_start_observation(self):
        m_host = "host1"
        m_station_host = self.configure_obs_future_mock(m_host)
        self.m_station_obs_future.side_effect = [m_station_host]

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, hosts=[m_host]
        )

        t_multi_observation.start()

        m_station_host.start.assert_called_once()

    def test_stop_observation(self):
        m_host = "host1"
        m_station_host = self.configure_obs_future_mock(m_host)
        self.m_station_obs_future.side_effect = [m_station_host]

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, hosts=[m_host]
        )

        t_multi_observation.stop()

        m_station_host.stop.assert_called_once()

    def setup_test(
        self,
        specification: dict,
        hosts: List[str],
        test: Callable[
            [
                multi_station_observation.MultiStationObservation,
                List[mock.Mock],
                List[str],
            ],
            None,
        ],
    ):
        """Dynamically configure a test scenario and execute to test() for conditions"""
        m_station_hosts = []
        for host in hosts:
            m_station_hosts.append(self.configure_obs_future_mock(host))

        self.m_station_obs_future.side_effect = m_station_hosts

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=specification, hosts=hosts
        )

        test(t_multi_observation, m_station_hosts, hosts)

    def case_observation_proxies(self, t_multi_observation, m_station_hosts, hosts):
        """Test case to use with :py:func:`~setup_test` to test observations property"""
        expected = {}
        for index, host in enumerate(hosts):
            expected[host] = m_station_hosts[index]

        results = t_multi_observation.observations
        self.assertEqual(expected, results)

    def test_observation_proxy(self):
        self.setup_test(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, ["host1"], self.case_observation_proxies
        )

    def test_observation_proxies(self):
        self.setup_test(
            SPEC_TWO_HOST_TWO_FIELD_DICT,
            ["host1", "m_host2"],
            self.case_observation_proxies,
        )

    def case_is_running(self, t_multi_observation, m_station_hosts, hosts):
        expected = {}
        for index, host in enumerate(hosts):
            expected[host] = True

        self.assertEqual(expected, t_multi_observation.is_running)

    def test_observation_is_running_true(self):
        self.setup_test(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, ["host1"], self.case_is_running
        )

    def test_observations_is_running_true(self):
        self.setup_test(
            SPEC_TWO_HOST_TWO_FIELD_DICT, ["host1", "host2"], self.case_is_running
        )

    def case_is_connected(self, t_multi_observation, m_station_hosts, hosts):
        expected = {}
        for index, host in enumerate(hosts):
            expected[host] = True

        self.assertEqual(expected, t_multi_observation.is_connected)
        self.assertTrue(t_multi_observation.all_connected)

    def test_observation_is_connected_true(self):
        self.setup_test(
            SPEC_SINGLE_HOST_SINGLE_FIELD_DICT, ["host1"], self.case_is_connected
        )

    def test_observations_is_connected_true(self):
        self.setup_test(
            SPEC_TWO_HOST_TWO_FIELD_DICT, ["host1", "host2"], self.case_is_connected
        )

    def case_no_connection(self, t_multi_observation, m_station_hosts, hosts):
        future = concurrent.futures.Future()
        future.set_exception(Exception("oh no"))

        for m_station in m_station_hosts:
            m_station.is_running = mock.PropertyMock(return_value=future)

        expected = {}
        for index, host in enumerate(hosts):
            expected[host] = False

        self.assertEqual(expected, t_multi_observation.is_running)

    def test_no_connection_is_running(self):
        hosts = ["host1", "host2"]
        m_station_hosts = []
        for host in hosts:
            m_station_hosts.append(
                self.configure_obs_future_mock(host, is_running=Exception("oh no"))
            )

        self.m_station_obs_future.side_effect = m_station_hosts

        t_multi_observation = multi_station_observation.MultiStationObservation(
            specifications=SPEC_TWO_HOST_TWO_FIELD_DICT, hosts=hosts
        )

        expected = {}
        for index, host in enumerate(hosts):
            expected[host] = False

        self.assertEqual(expected, t_multi_observation.is_running)
