"""Enhanced interfaces towards the station's Tango devices."""

#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

# inconsistent-return-statements
# pylint: disable=R1710

import ast
import logging
from functools import lru_cache

import numpy

from lofar_station_client._utils import sys_exit_no_tango

try:
    from tango import DevFailed, DeviceProxy
    from tango import ExtractAs
except ImportError:
    sys_exit_no_tango()

logger = logging.getLogger()


LAZY_DEVICE_PROXY_SLOTS = ["_create_instance", "connected", "_instance"]
LAZY_DEVICE_PROXY_MEMBERS = LAZY_DEVICE_PROXY_SLOTS + ["_connect"]


class LazyDeviceProxy:
    """A wrapper class that allows the regular DeviceProxy to only open the connection
    when needed and not during creation."""

    __slots__ = LAZY_DEVICE_PROXY_SLOTS

    def __init__(self, cls, *args, **kwargs):
        self._create_instance = lambda: cls(*args, **kwargs)
        self.connected = False
        self._instance = None

    def _connect(self):
        """Try to estabilish a connection when a device operation is called"""
        if not self.connected:
            try:
                self._instance = self._create_instance()
                self.connected = True
            except DevFailed as ex:
                self.connected = False
                reason = ex.args[0].desc.replace("\n", " ")
                logger.warning("LOFARDeviceProxy not connected: %s", reason)

    def __getattribute__(self, name):
        if name in LAZY_DEVICE_PROXY_MEMBERS:
            return object.__getattribute__(self, name)
        self._connect()
        return getattr(object.__getattribute__(self, "_instance"), name)

    def __delattr__(self, name):
        self._connect()
        delattr(object.__getattribute__(self, "_instance"), name)

    def __setattr__(self, name, value):
        if name in LAZY_DEVICE_PROXY_MEMBERS:
            object.__setattr__(self, name, value)
        else:
            self._connect()
            setattr(object.__getattribute__(self, "_instance"), name, value)

    def __nonzero__(self):
        self._connect()
        return bool(object.__getattribute__(self, "_instance"))

    def __str__(self):
        self._connect()
        return str(object.__getattribute__(self, "_instance"))

    def __repr__(self):
        self._connect()
        return repr(object.__getattribute__(self, "_instance"))


def lazy(cls):
    """A decorator that allows the regular DeviceProxy to only open the connection
    when needed and not during creation."""
    return lambda *args, **kwargs: LazyDeviceProxy(cls, *args, **kwargs)


@lazy
class LofarDeviceProxy(DeviceProxy):
    """A LOFAR-specific tango.DeviceProxy that provides
    a richer experience."""

    @lru_cache()
    def get_attribute_config(self, name):
        """Get cached attribute configurations, as they are not expected to change."""
        return super().get_attribute_config(name)

    @lru_cache()
    def get_attribute_shape(self, name):
        """Get the shape of the requested attribute, as a tuple."""
        config = self.get_attribute_config(name)

        if config.format and config.format[0] == "(":
            # For >2D arrays, the "format" property describes actual
            # the dimensions as a tuple (x, y, z, ...),
            # so reshape the value accordingly.
            shape = ast.literal_eval(config.format)
        elif config.max_dim_y > 0:
            # 2D array
            shape = (config.max_dim_y, config.max_dim_x)
        elif config.max_dim_x > 1:
            # 1D array
            shape = (config.max_dim_x,)
        else:
            # scalar
            shape = ()

        return shape

    def read_attribute(self, name, extract_as=ExtractAs.Numpy):
        """Read an attribute from the server."""
        attr = super().read_attribute(name, extract_as)

        # convert non-scalar values into their actual shape
        shape = self.get_attribute_shape(name)
        if shape != ():
            attr.value = attr.value.reshape(shape)

        return attr

    def write_attribute(self, name, value):
        """Write an attribute to the server."""
        config = self.get_attribute_config(name)
        shape = self.get_attribute_shape(name)

        # 2D arrays also represent arrays of higher dimensionality. reshape them
        # to fit their original Tango shape before writing.
        if shape != ():
            value = numpy.array(value)

            if value.shape != shape:
                raise ValueError(
                    f"Invalid shape. Given: {value.shape} Expected: {shape}"
                )

            if len(shape) > 2:
                # >2D arrays collapse into 2D
                value = value.reshape((config.max_dim_y, config.max_dim_x))

        return super().write_attribute(name, value)
