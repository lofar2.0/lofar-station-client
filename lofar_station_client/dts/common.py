# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Common methods shared across DTS not further classified"""


def devices_list_2_dict(devices_list, device_keys=None):
    """Convert a list of devices to a dictionary with known keys

    Devices are selected based on substring presence in the devices name

    :param devices_list: list of Tango devices
    :param device_keys: list of keys, should be a substring of the device names
    :return: dict of devices with known keys
    """
    if device_keys is None:
        device_keys = [
            "boot",
            "unb2",
            "recv",
            "sdp",
            "sst",
            "bst",
            "xst",
            "digitalbeam",
            "tilebeam",
            "beamlet",
            "apsct",
            "apspu",
        ]
    devices_dict = {}
    if isinstance(devices_list, dict):
        # if by accident devices_list is already a dict,
        # then simply return the dict
        return devices_list
    for device in devices_list:
        for device_key in device_keys:
            if device_key in device.name().lower():
                devices_dict[device_key] = device
    return devices_dict
