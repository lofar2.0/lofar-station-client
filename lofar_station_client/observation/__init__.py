#  Copyright (C) 2022 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0


"""
Contains classes to interact with observations
"""

from .multi_station_observation import MultiStationObservation
from .station_observation import StationObservation

__all__ = ["MultiStationObservation", "StationObservation"]
