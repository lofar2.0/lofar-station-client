#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""
Contains class for pythonic interface with observations
"""

import logging

from lofar_station_client.observation.station_observation_future import (
    StationObservationFuture,
)

logger = logging.getLogger()


class StationObservation:
    """
    This class provides a blocking pythonic interface to the
    ObservationControl and ObservationField devices on a single station.
    """

    TIMEOUT = 10

    def __init__(
        self,
        specification: dict,
        host: str = "databaseds.tangonet:10000",
    ):
        self.station = StationObservationFuture(specification, host)

        if not self.station.connected:
            raise RuntimeError(f"Was not able to connect with {host}")

    def start(self):
        """
        Starts this observation
        """
        self.station.start().result(timeout=self.TIMEOUT)

    def stop(self):
        """
        stops this observation
        """
        self.station.stop().result(timeout=self.TIMEOUT)

    @property
    def observation(self):
        """
        Returns the DeviceProxy of this observation
        """
        return self.station

    @property
    def is_running(self):
        """
        Returns whether this observation is currently running or not
        """
        return self.station.is_running.result(timeout=self.TIMEOUT)
